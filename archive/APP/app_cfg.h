#ifndef APP_CFG_H
#define APP_CFG_H


// task priority 
#define  STARTUP_TASK_PRIO             4
#define  AD_TASK_PRIO                  5
#define  ALT_TASK_PRIO                 6
#define  KEY_TASK_PRIO                 7
#define  COMM_TASK_PRIO                8
#define  SPK_TASK_PRIO                 9
#define  LED1_TASK_PRIO               10

// task stack size 
#define  STARTUP_TASK_STK_SIZE       256
#define  AD_TASK_STK_SIZE            256
#define  ALT_TASK_STK_SIZE           512
#define  KEY_TASK_STK_SIZE           256
#define  COMM_TASK_STK_SIZE          512
#define  SPK_TASK_STK_SIZE           256
#define  LED1_TASK_STK_SIZE          256


#define  SPK_NONE                      0
#define  SPK_WARN                      1
#define  SPK_BEEP                      2

#define  CH_NONE                       0
#define  CH_VAL                        1

#endif
