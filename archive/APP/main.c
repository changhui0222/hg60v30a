#include <General.h>


// setup task stack

static  OS_STK    StartupTaskSTK[STARTUP_TASK_STK_SIZE];   // 开辟起始任务堆栈区
static  OS_STK    AltTaskStk[ALT_TASK_STK_SIZE];          
static  OS_STK    KeyTaskStk[KEY_TASK_STK_SIZE];
static  OS_STK    CommTaskStk[COMM_TASK_STK_SIZE];   
static  OS_STK    AdTaskStk[AD_TASK_STK_SIZE]; 
static  OS_STK    SpkTaskStk[SPK_TASK_STK_SIZE]; 
static  OS_STK    Led1TaskStk[LED1_TASK_STK_SIZE];

OS_EVENT   *AltMbox;
OS_EVENT   *Comm1TxSem;
OS_EVENT   *SpkMbox;
OS_EVENT   *ChMbox;

INT8U      alt_state;
INT16U     alt_eo[8];

INT8U      SpkVol;
INT8U      SpkType;

INT8U      MarkTime;            // 打标时间
INT8U      MarkPre;             // 排气前打标
INT8U      MarkRxFlag;          // 收到打标命令
INT8U      MarkResFlag;         // 收到结果命令
INT8U      MarkIntFlag;         // 中断中打标标志

INT8U      IdleExhaust;

INT16U     AdDp;
INT16U     AdPs;
INT8U      AdCh;

INT16U     AdCnt     = 0;
INT8U      AdDpCnt   = 0; 
INT8U      AdPsCnt   = 0;
INT8U      AdDpState = 0;
INT16U     AdDpTmr   = 0;
INT16U     AdDpA     = 0;
INT16U     AdDpB     = 0;

INT8U      AdCntVal  = 0;
INT8U      AdPsTx    = 1;

ALT_TIME   alt_time;
ALT_TIME   alt_buf;

int main(void) 
{         
    OSInit();    // 系统初始化	
    
    AltMbox = OSMboxCreate((void *)0);    // Create MBOX for air leak test
    SpkMbox = OSMboxCreate((void *)0);
    ChMbox  = OSMboxCreate((void *)0);
    Comm1TxSem  = OSSemCreate(1);
    
    // FlashRdProtectEnable();
   	
    OSTaskCreate(StartupTask,(void*)0, &StartupTaskSTK[STARTUP_TASK_STK_SIZE-1], STARTUP_TASK_PRIO);

    OSStart();   // 任务开始运行  
    return 0;
}



static void StartupTask(void* p_arg)
{
    BSP_Init();
    
    OS_CPU_SysTickInit();                        // Initialize the SysTick.

    #if(OS_TASK_STAT_EN>0)                       // 使能ucos 的统计任务
        OSStatInit(); 
    #endif

    OSTaskCreate(AdTask,   (void*)0, &AdTaskStk[AD_TASK_STK_SIZE-1],     AD_TASK_PRIO);
    OSTaskCreate(AltTask,  (void*)0, &AltTaskStk[ALT_TASK_STK_SIZE-1],   ALT_TASK_PRIO);
    OSTaskCreate(KeyTask,  (void*)0, &KeyTaskStk[KEY_TASK_STK_SIZE-1],   KEY_TASK_PRIO);
    OSTaskCreate(CommTask, (void*)0, &CommTaskStk[COMM_TASK_STK_SIZE-1], COMM_TASK_PRIO);
    OSTaskCreate(SpkTask,  (void*)0, &SpkTaskStk[SPK_TASK_STK_SIZE-1],   SPK_TASK_PRIO);
    OSTaskCreate(Led1Task, (void*)0, &Led1TaskStk[LED1_TASK_STK_SIZE-1], LED1_TASK_PRIO);
    
    OSTaskDel(OS_PRIO_SELF);      //删除启动任务
}


static  void  AltTask (void *p_arg)
{ 
    INT8U    *r_msg; 
    INT8U     spk_msg;
    INT8U     ch_msg;
    INT8U     err;
    INT16U    i;
    INT8U     alt_step = 0;
    
    char      str[10];

    (void) p_arg;
    
    alt_state = ALT_NONE;

    while(1) {
      
        r_msg = (INT8U *)OSMboxPend(AltMbox, 0, &err);
        alt_state = *r_msg;
        
        if(alt_state == ALT_BUB_WARN)  
        {
            alt_step = 9;     // 涂泡压力不足报警，直接停止         
        }
        else                  // 除涂泡压力不足报警外，重新启动测试流程
        {
            alt_step = 0;
            AltExout(alt_step);        // 此处跟上位机扩展输出设置有冲突
        
            alt_time = alt_buf;        // 每次运行前跟新alt_time
        
            YoutOff(YOUT_GRP_C, YOUT_OK_LAMP | YOUT_NGP_LAMP | YOUT_NGN_LAMP);
            // YoutOff(YOUT_GRP_A,  BIT7 | BIT6 | BIT5);
            YoutOff(YOUT_GRP_D,  YOUT_OK_A | YOUT_NGP_A | YOUT_NGN_A); 
            YoutOff(YOUT_GRP_D,  YOUT_OK_B | YOUT_NGP_B | YOUT_NGN_B);
        
            // spk_msg = SPK_NONE;
            // OSMboxPost(SpkMbox, (void *)&spk_msg);
        }
            
        if(alt_state == ALT_START)
        {
            alt_step = 1;
            CommTxMsg(COMM1, "STOK");
            spk_msg = SPK_BEEP;
            OSMboxPost(SpkMbox, (void *)&spk_msg);
            OSTimeDlyHMSM(0, 0, 0, 10);
            YoutOff(YOUT_GRP_C, YOUT_STOP_LAMP | YOUT_BUB_LAMP );
            YoutOn(YOUT_GRP_C, YOUT_START_LAMP); 
            YoutOff(YOUT_GRP_B, BIT2 | BIT1 | BIT0);  // 关闭充气 平衡 排气阀
            
            MarkTime    = 0;
            MarkPre     = 0;
            MarkRxFlag  = 0;
            MarkResFlag = 0;
            MarkIntFlag = 0;
            
            Ad7705SelCh(AD_CH1);             // 选择测试直压 通道1
            
            CommTxMsg(COMM1, "CTOK");
            OSTimeDlyHMSM(0, 0, 0, 10);
        }
        else if(alt_state == ALT_BUB)
        {
            alt_step = 1;
            CommTxMsg(COMM1, "BUOK");
            spk_msg = SPK_BEEP;
            OSMboxPost(SpkMbox, (void *)&spk_msg);
            OSTimeDlyHMSM(0, 0, 0, 10);
            YoutOff(YOUT_GRP_C, YOUT_START_LAMP | YOUT_STOP_LAMP);
            YoutOn(YOUT_GRP_C, YOUT_BUB_LAMP);
            YoutOff(YOUT_GRP_B, BIT2 | BIT1 | BIT0);  // 关闭充气 平衡 排气阀
            
            Ad7705SelCh(AD_CH1);
            
            CommTxMsg(COMM1, "CTOK");
            OSTimeDlyHMSM(0, 0, 0, 10);
        }
        else if(alt_state == ALT_STOP)
        {
            alt_step = 9;
            // CommTxMsg(COMM1, "RTOK");
            
            spk_msg = SPK_BEEP;
            OSMboxPost(SpkMbox, (void *)&spk_msg);
            OSTimeDlyHMSM(0, 0, 0, 10);
            YoutOff(YOUT_GRP_C, YOUT_START_LAMP | YOUT_BUB_LAMP );
            YoutOn(YOUT_GRP_C, YOUT_STOP_LAMP);
            
            Ad7705SelCh(AD_CH1);         
        }
            
        if(alt_time.clamp1>0 && alt_step==1)
        {
            CommTxMsg(COMM1, "RP01");
            AltExout(alt_step);                          // 装夹1
            alt_step++;
            for(i=0; i<alt_time.clamp1*100; i++)
            {
                OSTimeDlyHMSM(0, 0, 0, 10); 
                r_msg = (INT8U *)OSMboxAccept(AltMbox); // 没收到数据接着执行
                if(*r_msg == ALT_STOP)
                {
                    alt_step = 9;
                    break;
                }
            }    
        }
        else if(alt_step < 9)
        {
            alt_step++;
        }
           
        if(alt_time.clamp2>0 && alt_step==2)
        {
            CommTxMsg(COMM1, "RP02");
            AltExout(alt_step);                         // 装夹2
            alt_step++;
            for(i=0; i<alt_time.clamp2*100; i++)
            {
                OSTimeDlyHMSM(0, 0, 0, 10); 
                r_msg = (INT8U *)OSMboxAccept(AltMbox); // 没收到数据接着执行
                if(*r_msg == ALT_STOP)
                {
                    alt_step = 9;
                    break;
                }
            }   
        }
        else if(alt_step < 9)
        {
            alt_step++;
        }
        
        if(alt_time.clamp3>0 && alt_step==3)
        {
            CommTxMsg(COMM1, "RP03");
            AltExout(alt_step);                         // 装夹3
            alt_step++;
            for(i=0; i<alt_time.clamp3*100; i++)
            {
                OSTimeDlyHMSM(0, 0, 0, 10); 
                r_msg = (INT8U *)OSMboxAccept(AltMbox); 
                if(*r_msg == ALT_STOP)
                {
                    alt_step = 9;
                    break;
                }
            }   
        }
        else if(alt_step < 9)
        {
            alt_step++;
        }
          
        if(alt_time.charge>0 && alt_step==4)
        {
            // CommTxMsg(COMM1, "CTOK");
            CommTxMsg(COMM1, "RP04");
            AltExout(alt_step);
            alt_step++;
            YoutOff(YOUT_GRP_B, BIT2);           // 关闭排气阀
            OSTimeDlyHMSM(0, 0, 0, 10);
            YoutOn(YOUT_GRP_B, BIT1 | BIT0);     // 打开充气 平衡阀
            for(i=0;i<alt_time.charge*100;i++)
            {
                OSTimeDlyHMSM(0, 0, 0, 10); 
                r_msg = (INT8U *)OSMboxAccept(AltMbox); // 没收到数据接着执行
                if(*r_msg == ALT_STOP)
                {
                    alt_step = 9;
                    break;
                }
            }
            if(alt_state != ALT_BUB)
            {
                YoutOff(YOUT_GRP_B, BIT0);          // 涂泡状态不关闭充气阀
            }
        }
        else if(alt_step < 9)
        {
            alt_step++;
        }
            
        if(alt_state != ALT_BUB)        // 非涂泡情况才执行
        {
            if(alt_time.depress>0 && alt_step==5)    // 泄压
            {
                CommTxMsg(COMM1, "RP05");
                AltExout(alt_step);      
                alt_step++;
                YoutOff(YOUT_GRP_B, BIT0);              // 关闭充气阀
                YoutOn(YOUT_GRP_B, BIT2 | BIT1);        // 打开平衡 排气阀
                
                for(i=0; i<alt_time.depress*10; i++)    //  泄压时间单位为100ms
                {
                    OSTimeDlyHMSM(0, 0, 0, 10); 
                    r_msg = (INT8U *)OSMboxAccept(AltMbox); // 没收到数据接着执行
                    if(*r_msg == ALT_STOP)
                    {
                        alt_step = 9;
                        break;
                    }
                }  
                YoutOff(YOUT_GRP_B, BIT2 | BIT1);     // 泄压完成后关闭平衡 排气阀
            }
            else if(alt_step < 9)
            {
                alt_step++;
            }
            
            if(alt_time.balance1>0 && alt_step==6)
            {
                CommTxMsg(COMM1, "RP06");                 // 平衡1 平衡阀已经打开
                AltExout(alt_step);
                alt_step++;
                for(i=0;i<alt_time.balance1*100;i++)
                {
                    OSTimeDlyHMSM(0, 0, 0, 10); 
                    r_msg = (INT8U *)OSMboxAccept(AltMbox); // 未收到数据接着执行
                    if(*r_msg == ALT_STOP)
                    {
                        alt_step = 9;
                        break;
                    }
                } 
                AdPsTx = 0;                         // 平衡1结束后停止发送直压
                YoutOff(YOUT_GRP_B, BIT1);          // 平衡1结束后关闭平衡阀
                OSTimeDlyHMSM(0, 0, 0, 200); 
            }
            else if(alt_step < 9)
            {
                alt_step++;
            }

            if(alt_time.balance2>0 && alt_step==7)
            {           
                CommTxMsg(COMM1, "RP07");                 // 平衡2
                OSTimeDlyHMSM(0, 0, 0, 800);
                AltExout(alt_step);
                alt_step++;
                
                Ad7705SelCh(AD_CH2);      // 选择测试差压 通道2
                
                for(i=0;i<alt_time.balance2*100;i++)
                {
                    OSTimeDlyHMSM(0, 0, 0, 10); 
                    r_msg = (INT8U *)OSMboxAccept(AltMbox); // 未收到数据接着执行
                    if(*r_msg == ALT_STOP)
                    {
                        alt_step = 9;
                        break;
                    }
                }
                
                AdDpA = 0;
                AdDpB = 0;
                AdDpState = 1;
                AdDpTmr   = 0;
                OSTimeDlyHMSM(0, 0, 0, 60);
               
                strcpy(&str[0], "TA");
                AdToAsc(AdDpA, &str[2]);
                CommTxMsg(COMM1, &str[0]);        // 平衡结束发送差压AD值A
               
                OSTimeDlyHMSM(0, 0, 0, 30);
                
            }
            else if(alt_step < 9)
            {
                alt_step++;
            }
            
            if(alt_time.test>0 && alt_step==8)
            {   
                CommTxMsg(COMM1, "RP08");           // 测试
                AltExout(alt_step);
                alt_step++;
                OSTimeDlyHMSM(0, 0, 0, 20);
                for(i=0;i<alt_time.test*10;i++)
                {
                    OSTimeDlyHMSM(0, 0, 0, 100); 
                    r_msg = (INT8U *)OSMboxAccept(AltMbox); // 未收到数据接着执行
                    if(*r_msg == ALT_STOP)
                    {
                        alt_step = 9;
                        break;
                    }
                }
                
                if(i>=alt_time.test*10)            // 测试未完成就停止  不发送TB命令
                {
                    strcpy(&str[0], "TB");
                    AdToAsc(AdDpB, &str[2]);
                    CommTxMsg(COMM1, &str[0]);    // 平衡结束发送差压AD值B
                    OSTimeDlyHMSM(0, 0, 0, 60);
                    MarkResFlag = 1;
                }
                
                Ad7705SelCh(AD_CH1);              // 选择测试直压 通道1 
                
                OSTimeDlyHMSM(0, 0, 0, 60);
                // AdPsTx = 1;
            }
            else if(alt_step < 9)
            {
                alt_step++;
            }
        }
        
        if(MarkResFlag==1)
        {
            MarkResFlag = 0;
            
            for(i=0;i<100;i++)
            {
                OSTimeDlyHMSM(0, 0, 0, 20); 
                if(MarkRxFlag==1)
                {
                    MarkRxFlag = 0;
                    break;
                }
            }
        
            if((MarkPre==1) && (MarkTime>0))
            {
                YoutOn(YOUT_GRP_D, YOUT_MARK_A);     // 启动打标
                for(i=0;i<MarkTime;i++)
                {
                    OSTimeDlyHMSM(0, 0, 0, 100); 
                }
                YoutOff(YOUT_GRP_D, YOUT_MARK_A);    // 关闭打标
                MarkPre  = 0;
                MarkTime = 0;
            }
            else if((MarkPre==0) && (MarkTime>0))
            {
                YoutOn(YOUT_GRP_D, YOUT_MARK_A);     // 启动打标
                MarkIntFlag = 1;          
            }
        }
   
        if(alt_time.blow>0 && alt_step==9)
        {
            CommTxMsg(COMM1, "RP09");       // 吹气
            AltExout(alt_step);                       
            alt_step++;
            alt_state = ALT_STOP;                    // 停止阶段
            YoutOn(YOUT_GRP_B, BIT2 | BIT1 | BIT0);  // 打开充气 平衡 排气阀
            OSTimeDlyHMSM(0, 0, alt_time.blow, 0);  
            // spk_msg = SPK_BEEP;
            // OSMboxPost(SpkMbox, (void *)&spk_msg); 
            OSTimeDlyHMSM(0, 0, 0, 20);          
        }
        else
        {
            alt_step++;
        }

        if(alt_time.exhaust>0 && alt_step==10)
        { 
            CommTxMsg(COMM1, "RP10");
            AltExout(alt_step);
            alt_step++;
            alt_state = ALT_STOP;               // 停止阶段
            
            Ad7705SelCh(AD_CH1);
            AdPsTx = 1;
            
            YoutOff(YOUT_GRP_B, BIT0);          // 关闭充气阀
            OSTimeDlyHMSM(0, 0, 0, 10);
            YoutOn(YOUT_GRP_B, BIT2 | BIT1);    // 打开平衡 排气阀
            YoutOff(YOUT_GRP_C, YOUT_START_LAMP | YOUT_BUB_LAMP );
            YoutOn(YOUT_GRP_C, YOUT_STOP_LAMP);
            OSTimeDlyHMSM(0, 0, alt_time.exhaust, 0);
            // YoutOff(YOUT_GRP_B, BIT2 | BIT1);          // 关闭平衡 排气阀
            
            if(IdleExhaust==0)
            {
                YoutOff(YOUT_GRP_B, BIT2 | BIT1 | BIT0);  // 关闭充气 平衡 排气阀
            }
        }
        else
        {
            alt_step++;
        }           
            
        if(alt_time.unload1>0 && alt_step==11)
        {
            CommTxMsg(COMM1, "RP11");                 // 卸夹1
            AltExout(alt_step);                    
            alt_step++;
            OSTimeDlyHMSM(0, 0, alt_time.unload1, 0);        
        }
        else
        {
             alt_step++;
        }
            
        if(alt_time.unload2>0 && alt_step==12)
        {
            CommTxMsg(COMM1, "RP12");                 // 卸夹2
            AltExout(alt_step);       
            alt_step++;
            OSTimeDlyHMSM(0, 0, alt_time.unload2, 0);           
        }
        else
        {
             alt_step++;
        }
        
        if(alt_time.unload3>0 && alt_step==13)
        {
            CommTxMsg(COMM1, "RP13");                 // 卸夹3
            AltExout(alt_step);       
            alt_step++;
            OSTimeDlyHMSM(0, 0, alt_time.unload3, 0);           
        }

        if(alt_state != ALT_BUB)
        {
            OSTimeDlyHMSM(0, 0, 0, 10);
            CommTxMsg(COMM1, "RP00");                 // 返回待机状态
            alt_step  = 0;
            AltExout(alt_step);                    
            alt_state = ALT_NONE;
            
            ch_msg = CH_VAL;
            OSMboxPost(ChMbox, (void *)&ch_msg);
            
            OSTimeDlyHMSM(0, 0, 0, 10);
            // YoutOff(YOUT_GRP_B, BIT2 | BIT1 | BIT0);  // 关闭充气 平衡 排气阀
            YoutOff(YOUT_GRP_C, YOUT_START_LAMP | YOUT_BUB_LAMP | YOUT_STOP_LAMP);
            AdPsTx = 1;
        }
    }
}


/*
*********************************************************************************************************
*                                             App_TaskPB()
*
* Description : This task monitors the state of the push buttons and passes messages to AppTaskUserIF()
*
* Argument(s) : p_arg   is the argument passed to 'App_TaskPB()' by 'OSTaskCreate()'.
*
* Return(s)  : none.
*
* Caller(s)  : This is a task.
*
* Note(s)    : none.
*********************************************************************************************************
*/

static  void  KeyTask (void *p_arg)
{
    BOOLEAN  key_start_prev;
    BOOLEAN  key_start;
    BOOLEAN  key_stop_prev;
    BOOLEAN  key_stop;
    BOOLEAN  key_bub_prev;
    BOOLEAN  key_bub;
    
    INT8U    alt_msg;
    INT8U    spk_msg;

    (void) p_arg;

    key_start_prev = DEF_FALSE;
    key_stop_prev  = DEF_FALSE;
    key_bub_prev   = DEF_FALSE;
    
    alt_msg        = DEF_FALSE;
    spk_msg        = DEF_FALSE;

    while(1) {
        key_start = KeyGetStatus(KEY_START1) | KeyGetStatus(KEY_START2) | KeyGetStatus(KEY_START_A) ;
        key_stop  = KeyGetStatus(KEY_STOP1)  | KeyGetStatus(KEY_STOP2)  | KeyGetStatus(KEY_STOP_AB) ;
        key_bub   = KeyGetStatus(KEY_BUB1)   | KeyGetStatus(KEY_BUB2)   | KeyGetStatus(KEY_BUB_A) ;

        if((key_start == DEF_FALSE) && (key_start_prev == DEF_TRUE)) 
        { 
            if(alt_state == ALT_NONE)           // 空闲状态下可以开始测试
            {
                alt_msg = ALT_START;
                OSMboxPost(AltMbox, (void *)&alt_msg);
                spk_msg = SPK_BEEP;
                OSMboxPost(SpkMbox, (void *)&spk_msg);
            }
        }
        
        if((key_stop == DEF_FALSE) && (key_stop_prev == DEF_TRUE))
        {
            if(alt_state != ALT_STOP)
            {
                alt_msg = ALT_STOP;
                OSMboxPost(AltMbox, (void *)&alt_msg);
                spk_msg = SPK_BEEP;
                OSMboxPost(SpkMbox, (void *)&spk_msg);
            }
            CommTxMsg(COMM1, "RTOK");
        } 
        
        if((key_bub == DEF_FALSE) && (key_bub_prev == DEF_TRUE)) 
        {
            if(alt_state == ALT_NONE)           // 空闲状态下可以开始涂泡
            {
                alt_msg = ALT_BUB;
                OSMboxPost(AltMbox, (void *)&alt_msg);
                spk_msg = SPK_BEEP;
                OSMboxPost(SpkMbox, (void *)&spk_msg);
            }
        }
        
        key_start_prev = key_start;
        key_stop_prev  = key_stop;
        key_bub_prev   = key_bub;

        OSTimeDlyHMSM(0, 0, 0, 80);
        // msg = 1;
        // OSMboxPost(AltMbox, (void *)&msg);
    }
}

static  void  CommTask (void *p_arg)
{ 
    INT8U  alt_msg;
    INT8U  spk_msg;
    INT8U  c;
    INT8U  i;
    INT8U  temp;
    INT8U  check;
    INT8U  rx_state;
    char   buf[60];
    char   *s;
    char   *pstr;
    INT8U  err;
    
    (void) p_arg;
    
    alt_msg  = DEF_FALSE;
    spk_msg  = DEF_FALSE;
    
    rx_state = 0;
    check    = 0;
    
    CommInit();
    CommCfgPort(COMM1, 19200, 8, COMM_PARITY_NONE, 1);
    CommRxIntEn(COMM1);
    
    while(1) 
    {  
        c = CommGetChar(COMM1, 0, &err);
        LedOn(LED2);
        
        if(rx_state==0)
        {
            if(c == 0x02)
            {
                rx_state++;
                s     = &buf[0];
               *s++   = c;
                check = c;  
                
            }
        }
        else if(rx_state==1)
        {
            *s++   = c;
            check ^= c; 
            if(c==0x03)
            {
                rx_state++;
            }
        }
        else if(rx_state==2)
        {
            *s = c;
            rx_state = 0;
            if(check==c)
            {
                //*(s-1) = 0;  // 0x03转换成0，作为字符串结尾
                pstr = &buf[1];
                if(strncmp(pstr, "ST", 2)==0)
                {    
                    alt_msg = ALT_START;
                    OSMboxPost(AltMbox, (void *)&alt_msg);
                }
                else if(strncmp(pstr, "RT", 2)==0)
                {
                    alt_msg = ALT_STOP;
                    OSMboxPost(AltMbox, (void *)&alt_msg);
                }
                else if(strncmp(pstr, "BU", 2)==0)
                {
                    alt_msg = ALT_BUB;
                    OSMboxPost(AltMbox, (void *)&alt_msg);
                }
                
                else if(strncmp(pstr, "MT", 2)==0)
                {
                    s        = &buf[3];
                    MarkTime = (*s - '0')*10;            // 打标时间 秒
                    s++;
                    MarkTime = MarkTime + (*s - '0');    // 打标时间 百毫秒
                    s++;
                    MarkPre  = (*s - '0');
                    MarkRxFlag = 1;
                    // YoutOn(YOUT_GRP_D, YOUT_MARK_A);     // 启动打标
                }
                
                else if(strncmp(pstr, "NG", 2)==0)
                {
                    MarkResFlag = 1;       // 指示打标有效
                    s      = &buf[3];
                    temp   = *s - '0';
                    if(temp==0)            // OK
                    {
                        YoutOff(YOUT_GRP_C, YOUT_NGP_LAMP | YOUT_NGN_LAMP);
                        YoutOn(YOUT_GRP_C,  YOUT_OK_LAMP);
                        // YoutOff(YOUT_GRP_A, BIT7 | BIT6);     // 关闭外部扩展+NG -NG
                        // YoutOn(YOUT_GRP_A,  BIT5);            // 打开外部扩展OK
                        YoutOff(YOUT_GRP_D,  YOUT_NGP_A | YOUT_NGN_A);
                        YoutOn(YOUT_GRP_D,  YOUT_OK_A);

                    }
                    else if(temp==1)       // +NG
                    {
                        YoutOff(YOUT_GRP_C, YOUT_OK_LAMP | YOUT_NGN_LAMP);
                        YoutOn(YOUT_GRP_C,  YOUT_NGP_LAMP);
                        // YoutOff(YOUT_GRP_A, BIT7 | BIT5);     // 关闭外部扩展-NG OK
                        // YoutOn(YOUT_GRP_A,  BIT6);            // 打开外部扩展+NG
                        YoutOff(YOUT_GRP_D,  YOUT_NGN_A | YOUT_OK_A);
                        YoutOn(YOUT_GRP_D,  YOUT_NGP_A);
                        
                        spk_msg = SPK_WARN;
                        OSMboxPost(SpkMbox, (void *)&spk_msg);
                    }
                    
                    else if(temp==2)       // -NG
                    {
                        YoutOff(YOUT_GRP_C, YOUT_OK_LAMP | YOUT_NGP_LAMP);
                        YoutOn(YOUT_GRP_C,  YOUT_NGN_LAMP);
                        // YoutOff(YOUT_GRP_A, BIT6 | BIT5);    // 关闭外部扩展+NG OK
                        // YoutOn(YOUT_GRP_A,  BIT7);           // 打开外部扩展-NG
                        YoutOff(YOUT_GRP_D,  YOUT_NGP_A | YOUT_OK_A);
                        YoutOn(YOUT_GRP_D,  YOUT_NGN_A);
                        
                        spk_msg = SPK_WARN;
                        OSMboxPost(SpkMbox, (void *)&spk_msg);
                    }
                    
                    else if(temp==3)       // +超量程
                    {
                        YoutOff(YOUT_GRP_C, YOUT_OK_LAMP | YOUT_NGN_LAMP);
                        YoutOn(YOUT_GRP_C,  YOUT_NGP_LAMP);
                        // YoutOff(YOUT_GRP_A, BIT7 | BIT5);   // 关闭外部扩展-NG OK
                        // YoutOn(YOUT_GRP_A,  BIT6);          // 打开外部扩展+NG
                        YoutOff(YOUT_GRP_D,  YOUT_NGN_A | YOUT_OK_A);
                        YoutOn(YOUT_GRP_D,  YOUT_NGP_A);
                        
                        alt_msg = ALT_STOP;
                        OSMboxPost(AltMbox, (void *)&alt_msg);
                        
                        spk_msg = SPK_WARN;
                        OSMboxPost(SpkMbox, (void *)&spk_msg);
                    }
                    
                    else if(temp==4)       // -超量程
                    {
                        YoutOff(YOUT_GRP_C, YOUT_OK_LAMP | YOUT_NGP_LAMP);
                        YoutOn(YOUT_GRP_C,  YOUT_NGN_LAMP);
                        // YoutOff(YOUT_GRP_A, BIT6 | BIT5);   // 关闭外部扩展+NG OK
                        // YoutOn(YOUT_GRP_A,  BIT7);          // 打开外部扩展-NG
                        YoutOff(YOUT_GRP_D,  YOUT_NGP_A | YOUT_OK_A);
                        YoutOn(YOUT_GRP_D,  YOUT_NGN_A);
                        
                        alt_msg = ALT_STOP;    
                        OSMboxPost(AltMbox, (void *)&alt_msg);
                        
                        spk_msg = SPK_WARN;
                        OSMboxPost(SpkMbox, (void *)&spk_msg);
                    }
                    
                    else if(temp==5)       // 大漏
                    {
                        YoutOff(YOUT_GRP_C, YOUT_OK_LAMP);
                        YoutOn(YOUT_GRP_C,  YOUT_NGP_LAMP | YOUT_NGN_LAMP);
                        // YoutOff(YOUT_GRP_A, BIT5);          // 关闭外部扩展OK
                        // YoutOn(YOUT_GRP_A,  BIT7 | BIT6);   // 打开外部扩展-NG +NG
                        YoutOff(YOUT_GRP_D,  YOUT_OK_A);
                        YoutOn(YOUT_GRP_D,  YOUT_NGP_A | YOUT_NGN_A);
                        
                        if(alt_state != ALT_BUB)
                        {
                            alt_msg = ALT_STOP;    
                            OSMboxPost(AltMbox, (void *)&alt_msg);
                        }
                        else
                        {
                            alt_msg = ALT_BUB_WARN;    
                            OSMboxPost(AltMbox, (void *)&alt_msg); 
                        }
                        
                        spk_msg = SPK_WARN;
                        OSMboxPost(SpkMbox, (void *)&spk_msg);
                    }
                    
                }
                else if(strncmp(pstr, "EO", 2)==0)
                {
                    s       = &buf[3];
                    temp    = (*s++) - '0';
                    for(i=0; i<14; i++)
                    {
                        alt_eo[temp] = alt_eo[temp] >> 1;
                        if(*s == '1')
                        {
                            alt_eo[temp] = alt_eo[temp] | 0x2000; 
                        }
                        s++;
                    }
                    // temp = 1;
                    // alt_eo[temp] = alt_eo[temp] >> 3;
                }
                
                else if(strncmp(pstr, "WV", 2)==0)
                {
                    s       = &buf[3];
                    SpkVol = *s - '0';                 // 后面需添加音量写入函数
                }
                else if(strncmp(pstr, "IE", 2)==0)
                {
                    s       = &buf[3];
                    IdleExhaust = *s - '0';             // 待机排气设定 1执行待机排气
                }
                else if(strncmp(pstr, "WP", 2)==0)
                {
                    s = &buf[3];
                    alt_buf.clamp1   = CommAscToData(s);
                    s += 4;
                    alt_buf.clamp2   = CommAscToData(s);
                    s += 4;
                    alt_buf.clamp3   = CommAscToData(s);
                    s += 4;
                    alt_buf.charge   = CommAscToData(s);
                    s += 4;
                    alt_buf.depress  = CommAscToData(s);
                    s += 4;
                    alt_buf.balance1 = CommAscToData(s);
                    s += 4;
                    alt_buf.balance2 = CommAscToData(s);
                    s += 4;
                    alt_buf.test     = CommAscToData(s);
                    s += 4;
                    alt_buf.blow     = CommAscToData(s);
                    s += 4;
                    alt_buf.exhaust  = CommAscToData(s);
                    s += 4;
                    alt_buf.unload1  = CommAscToData(s);
                    s += 4;
                    alt_buf.unload2  = CommAscToData(s);
                    s += 4;
                    alt_buf.unload3  = CommAscToData(s);
                    
                }
                
                else if(strncmp(pstr, "CH", 2)==0)    // 选频命令
                {
                    temp = ChGetEn();
                    if(temp==0)
                    {
                        CommTxMsg(COMM1, "CH000");    // 发送通道未使能
                    }
                    else
                    {
                        temp = ChGetCh();
                        strcpy(&buf[0], "CH100");
                        buf[3] = temp / 10 + '0';
                        buf[4] = temp % 10 + '0';
                        CommTxMsg(COMM1, &buf[0]);
                    }
                  
                }
                else
                {
                    // CommTxMsg(COMM1, "ER2");    // 无此命令
                }
            }
            else
            {
                // CommTxMsg(COMM1, "ER1");        // 命令校验错误
            }
        } 
    }
}

// LED任务
static void Led1Task(void* p_arg)
{
    INT8U  *msg;
    INT8U  ch_en;
    INT8U  ch_en_pre;
    INT8U  ch;
    INT8U  ch_pre;
    
    char   buf[6];
    
    
    GPIO_InitTypeDef GPIO_InitStructure;

    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
 
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9 | GPIO_Pin_10;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_Init(GPIOA, &GPIO_InitStructure);
    
    // YoutOn(YOUT_GRP_C, YOUT_OK_LAMP | YOUT_NGN_LAMP | YOUT_NGP_LAMP); 
    YoutOff(YOUT_GRP_C, YOUT_OK_LAMP | YOUT_NGN_LAMP | YOUT_NGP_LAMP);
    YoutOff(YOUT_GRP_C, YOUT_START_LAMP | YOUT_STOP_LAMP | YOUT_BUB_LAMP);
    
    while(1)
    {
        LedTog(LED1);
        LedOff(LED2);
        FlashRdProtectDisable();
	    OSTimeDly(100);
        if(MarkIntFlag==1)
        {
            if(MarkTime>0)
            {
                MarkTime--;
            }
            else if(MarkTime==0)
            {
                MarkIntFlag = 0;
                YoutOff(YOUT_GRP_D, YOUT_MARK_A);    // 时间到 关闭打标
            }
        }
        // YoutOn(YOUT_GRP_C, YOUT_OK_LAMP | YOUT_NGN_LAMP | YOUT_NGP_LAMP); 
        // OSTimeDly(100); 
        // YoutOff(YOUT_GRP_C, YOUT_OK_LAMP | YOUT_NGN_LAMP | YOUT_NGP_LAMP );
        
        ch_en = ChGetEn();
        ch    = ChGetCh();
        
        if(((ch_en!=ch_en_pre) || (ch!=ch_pre)) && (alt_state == ALT_NONE) )  // 空闲状态下 选频发生变化 发送一次
        {
            ch_en_pre = ch_en;
            ch_pre    = ch;
            
            strcpy(&buf[0], "CH000");
            buf[2] = ch_en   + '0';
            buf[3] = ch / 10 + '0';
            buf[4] = ch % 10 + '0';
            CommTxMsg(COMM1, &buf[0]);
        }
        
        msg = (INT8U *)OSMboxAccept(ChMbox);
        if(*msg == CH_VAL)
        {
            strcpy(&buf[0], "CH000");
            buf[2] = ch_en   + '0';
            buf[3] = ch / 10 + '0';
            buf[4] = ch % 10 + '0';
            CommTxMsg(COMM1, &buf[0]);  
        }  
        
        CheckUid();
    } 
} 


// LED任务
static void SpkTask(void* p_arg)
{
    INT8U  *msg;
  
    INT8U  spk_state;
    INT8U  spk_cnt;
  
    (void) p_arg;
    
    spk_state = 0;
    spk_cnt   = 0;
    SpkVol    = 1;
    
    while(1)
    {
        OSTimeDlyHMSM(0, 0, 0, 100);
        msg = (INT8U *)OSMboxAccept(SpkMbox);
        if(msg != (void *)0)
        {
            if(*msg == SPK_WARN)
            {
                spk_state = 1;
                spk_cnt   = 0;
            }
            else if(*msg == SPK_BEEP)
            {
                spk_state = 0;
                SpkOn(SPK_BEEP, 2);
                OSTimeDlyHMSM(0, 0, 0, 100);
                SpkOff();
            }
            else if(*msg == SPK_NONE)
            {
                spk_state = 0;
                SpkOff();
            }
        }
        else 
        {
            if(spk_state == 1)
            {
                SpkOn(SPK_WARN, SpkVol);
                OSTimeDlyHMSM(0, 0, 0, 300);
                SpkOff();
                OSTimeDlyHMSM(0, 0, 0, 300);
                spk_cnt++;
                if(spk_cnt==20)
                {
                    spk_cnt   = 0;
                    spk_state = 0;
                }
            }
        }
    } 
}


static void AdTask(void* p_arg)
{
    char   str[10];
    
    (void) p_arg;
    
    Ad7705Init();
      
    AdDp  =  0;
    AdPs  =  0;
    Ad7705SelCh(AD_CH1);             // 选择测试直压 通道1
    
    while(1)
    {
        // LedTog(LED2);
        OSTimeDlyHMSM(0, 0, 0, 300);
        
        if(AdCh == AD_CH2 && AdCntVal==1)
        {
            strcpy(&str[0], "TR");
            AdToAsc(AdDp, &str[2]);
            CommTxMsg(COMM1, &str[0]);        // 发送差压AD值
        }
        else if(AdCh == AD_CH1 && AdCntVal==1 && AdPsTx == 1)
        {
            strcpy(&str[0], "PR");
            AdToAsc(AdPs, &str[2]);
            CommTxMsg(COMM1, &str[0]);        // 发送测试压AD值
        }
        
    } 
}


void CommTxMsg(INT8U comm_id, char *pstr)
{
    INT8U  err;
    INT8U  check;
  
    err = COMM_NO_ERR;
    
    check = 0x02;
    
    err = CommPutChar(comm_id, 0x02, 0);
    while(*pstr && err == COMM_NO_ERR)
    {
        check ^= *pstr;
        err = CommPutChar(comm_id,*pstr++, 0);
    } 
    check ^= 0x03;
    err = CommPutChar(comm_id, 0x03, 0);
    err = CommPutChar(comm_id, check, 0);
}


void delay_us(u32 time)
{    
   u32 i=0;  
   while(time--)
   {
      i=6;  
      while(i--) ;    
   }
}

void delay_ms(u32 time)
{    
    u32 i=0;  
    while(time--)
    {
       i=8000;  
       while(i--) ;    
    }
}

void  AdToAsc (INT16U ad_val, char *s)
{
    INT16U  temp;
    
    strcpy(s, "00000");             // Create the template for the selected format
    s[0] = ad_val / 10000 + '0';    // Convert data to ASCII
    temp = ad_val % 10000;
    s[1] = temp   / 1000  + '0';
    temp = temp   % 1000;
    s[2] = temp   / 100   + '0';
    temp = temp   % 100;
    s[3] = temp   / 10    + '0';
    s[4] = temp   % 10    + '0';
}

INT16U CommAscToData(char *s)
{
    INT16U temp;
    
    temp  = ((*s++)-'0')*1000;
    temp += ((*s++)-'0')*100;
    temp += ((*s++)-'0')*10;
    temp += ((*s++)-'0');
    
    return temp;
}


void AltExout(INT16U alt_step)
{
    INT8U  i;
  
   // for(i=0; i<8; i++)  
   for(i=0; i<8; i++)             //  5 6 7输出已用于OK +NG -NG输出
   {
       if((alt_eo[i] & (0x0001<<alt_step)) != 0)
       {
           YoutOn(YOUT_GRP_A, (0x01<<i));
       }
       else
       {
           YoutOff(YOUT_GRP_A, (0x01<<i));
       }
   }
}



