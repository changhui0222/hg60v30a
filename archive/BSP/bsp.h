#ifndef  BSP_H 
#define  BSP_H


#ifdef   BSP_GLOBALS
#define  BSP_EXT
#else
#define  BSP_EXT  extern
#endif

// #include  <cpu.h>
#include  <lib_def.h>
#include "stm32f10x.h" 
#include "ucos_ii.h"


#define    KEY_START1         0
#define    KEY_STOP1          1
#define    KEY_BUB1           2
#define    KEY_RSV1           3

#define    KEY_START2         4
#define    KEY_STOP2          5
#define    KEY_BUB2           6
#define    KEY_RSV2           7

#define    KEY_START_A        8
#define    KEY_START_B        9
#define    KEY_STOP_AB       10
#define    KEY_BUB_A         11
#define    KEY_BUB_B         12
#define    KEY_RSV3          13
#define    KEY_RSV4          14
#define    KEY_RSV5          15


#define    YOUT_GRP_A         0
#define    YOUT_GRP_B         1
#define    YOUT_GRP_C         2
#define    YOUT_GRP_D         3
#define    YOUT_GRP_ALL    0xFF

#define    LED1               0
#define    LED2               1
#define    LED_ALL         0xFF

#define    BIT0             0x01
#define    BIT1             0x02
#define    BIT2             0x04
#define    BIT3             0x08
#define    BIT4             0x10
#define    BIT5             0x20
#define    BIT6             0x40
#define    BIT7             0x80

#define    YOUT_START_LAMP   BIT7
#define    YOUT_STOP_LAMP    BIT6
#define    YOUT_BUB_LAMP     BIT5

#define    YOUT_OK_LAMP      BIT0
#define    YOUT_NGP_LAMP     BIT1
#define    YOUT_NGN_LAMP     BIT2

#define    YOUT_MARK_A       BIT7
#define    YOUT_NGP_A        BIT6
#define    YOUT_NGN_A        BIT5
#define    YOUT_OK_A         BIT4
#define    YOUT_MARK_B       BIT3
#define    YOUT_NGP_B        BIT2
#define    YOUT_NGN_B        BIT1
#define    YOUT_OK_B         BIT0

#define    AD_CH_NONE          0
#define    AD_CH1              1
#define    AD_CH2              2

BSP_EXT INT8U YoutGrpData[4];

void systick_init(void); //��������
void BSP_Init(void);

void    YoutOn  (INT8U grp_id, INT8U yout_id);
void    YoutOff (INT8U grp_id, INT8U yout_id);

void    LedOn  (INT8U led_id);
void    LedOff (INT8U led_id);
void    LedTog (INT8U led_id);

void    Delay(INT8U i);
void    delay_us(u32 time);
void    delay_ms(u32 time);

INT8U   CommCfgPort (INT8U ch, INT16U baud, INT8U bits, INT8U parity, INT8U stops);
void    CommRxIntEn (INT8U ch);

BOOLEAN KeyGetStatus (INT8U  key_id);
void    Ad7705Init(void);
INT16U  Ad7705GetVal(INT8U ad_ch);
INT16U  Ad7705RdReg(INT8U ad_ch);

INT16U   Ad7705Rd(void);
void     Ad7705Wr(INT8U wr_data);
void     Ad7705SelCh(INT8U ad_ch);

INT8U    ChGetEn(void);
INT8U    ChGetCh(void);

void     SpkOn(INT8U spk_type, INT8U spk_vol);
void     SpkOff(void);

void     FlashRdProtectInit();
void     FlashRdProtectEnable();
void     FlashRdProtectDisable();



 
#endif
