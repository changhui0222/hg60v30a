
#include "general.h"
//#include <bsp.h>
//#include "includes.h"


// setup task stack
static  OS_STK    StartupTaskSTK[STARTUP_TASK_STK_SIZE];   // 开辟起始任务堆栈区         
static  OS_STK    NbTaskStk[NB_TASK_STK_SIZE];
static  OS_STK    Rx1TaskStk[RX1_TASK_STK_SIZE]; 
static  OS_STK    Rx3TaskStk[RX3_TASK_STK_SIZE]; 
static  OS_STK    AdTaskStk[AD_TASK_STK_SIZE];
static  OS_STK    CamTaskStk[CAM_TASK_STK_SIZE];
static  OS_STK    KeyTaskStk[KEY_TASK_STK_SIZE];
static  OS_STK    LedTaskStk[LED_TASK_STK_SIZE];


OS_EVENT   *AltMbox;
OS_EVENT   *Comm1TxSem;
OS_EVENT   *SpkMbox;
OS_EVENT   *ChMbox;

OS_EVENT   *Rx3Mbox;

OS_EVENT   *NbMbox;
OS_EVENT   *CamMbox;

OS_EVENT   *CamSem;


INT16U     AdTemp;
INT16U     AdLevel;

INT16U     ADVal[10];
//char       DBStr[10]  = "DB00000";
char       DiStr[10]  = "DI000000";

INT8U      DoorFlag = 0;
INT8U      CamFlag  = 0;

char       nb_imei[20] = "869976030079405";				// 15 byte
char       nb_sim[30]  = "898607B61918C0058563";		// 20 byte
char       nb_csq[10]  = "00";		// 20 byte

INT32U     NbId = 0xA5A5A5A5;

INT8U      csq = 0;

INT8U      NsoFlag = 0;

INT8U      AdCntVal  = 0;
INT8U      AdPsTx    = 1;

INT8U      LenFlag    = 0;
INT8U      len1       = 0;
INT16U     len2       = 0;

INT8U      CamLenBuf[2] = {0x00, 0x00};
INT16U     CamLen       = 0;

INT8U      CamBuf[1005];
INT8U      CamTxFlag;

INT8U      CamTxCnt;

ALT_TIME   alt_time;
ALT_TIME   alt_buf;

INT16U TempSet  = 2073;    //2073-39度 2041-38度

const INT16U VERSION        = 1801;
const INT16U AD_TEMP_DELTA  = 10;    // 对应0.3度
const INT16U AD_LEVEL_MIN   = 2700;  // 对应850mm
const INT16U AD_TEMP_MIN    = 500;

int main(void) 
{         
    OSInit();    // 系统初始化	
    
    AltMbox = OSMboxCreate((void *)0);    // Create MBOX for air leak test
    SpkMbox = OSMboxCreate((void *)0);
    ChMbox  = OSMboxCreate((void *)0);
	
	NbMbox  = OSMboxCreate((void *)0);
    //CamMbox = OSMboxCreate((void *)0);
	//CamSem  = OSSemCreate(0);
    //Comm1TxSem  = OSSemCreate(1);
    
    // FlashRdProtectEnable();
   	
    OSTaskCreate(StartupTask,(void*)0, &StartupTaskSTK[STARTUP_TASK_STK_SIZE-1], STARTUP_TASK_PRIO);

    OSStart();   // 任务开始运行  
    return 0;
}



static void StartupTask(void* p_arg)
{
    BspInit();
    
    OS_CPU_SysTickInit();                        // Initialize the SysTick.

    #if(OS_TASK_STAT_EN>0)                       // 使能ucos统计任务
        OSStatInit(); 
    #endif

    OSTaskCreate(NbTask,   (void*)0, &NbTaskStk[NB_TASK_STK_SIZE-1],     NB_TASK_PRIO);
	OSTaskCreate(AdTask,   (void*)0, &AdTaskStk[AD_TASK_STK_SIZE-1],     AD_TASK_PRIO);
    OSTaskCreate(KeyTask,  (void*)0, &KeyTaskStk[KEY_TASK_STK_SIZE-1],   KEY_TASK_PRIO);
    OSTaskCreate(Rx3Task,  (void*)0, &Rx3TaskStk[RX3_TASK_STK_SIZE-1],   RX3_TASK_PRIO);
    OSTaskCreate(Rx1Task,  (void*)0, &Rx1TaskStk[RX1_TASK_STK_SIZE-1],   RX1_TASK_PRIO);
	OSTaskCreate(CamTask,  (void*)0, &CamTaskStk[CAM_TASK_STK_SIZE-1],   CAM_TASK_PRIO);
    OSTaskCreate(LedTask,  (void*)0, &LedTaskStk[LED_TASK_STK_SIZE-1],   LED_TASK_PRIO);

    
    OSTaskDel(OS_PRIO_SELF);      //删除启动任务
}


/*
*********************************************************************************************************
*                                             App_TaskPB()
*
* Description : This task monitors the state of the push buttons and passes messages to AppTaskUserIF()
*
* Argument(s) : p_arg   is the argument passed to 'App_TaskPB()' by 'OSTaskCreate()'.
*
* Return(s)  : none.
*
* Caller(s)  : This is a task.
*
* Note(s)    : none.
*********************************************************************************************************
*/

static  void  KeyTask (void *p_arg)
{
    BOOLEAN  key_door_prev;
    BOOLEAN  key_door;
    BOOLEAN  key_shell_prev;
    BOOLEAN  key_shell;
    BOOLEAN  key_power_prev;
    BOOLEAN  key_power;
    BOOLEAN  key_rsv1_prev;
    BOOLEAN  key_rsv1;
    BOOLEAN  key_rsv2_prev;
    BOOLEAN  key_rsv2;
    BOOLEAN  key_rsv3_prev;
    BOOLEAN  key_rsv3;

    (void)   p_arg;

    key_door_prev   = DEF_FALSE;
    key_shell_prev  = DEF_FALSE;
    key_power_prev  = DEF_FALSE;
    key_rsv1_prev   = DEF_FALSE;
    key_rsv2_prev   = DEF_FALSE;
    key_rsv3_prev   = DEF_FALSE;
    
    INT8U  key_flag = 0;
  
    if(KeyGetStatus(KEY_DOOR))
    {
        DiStr[2] = '0';
    }
    else
    {
        DiStr[2] = '1';
    }
    DiStr[3] = KeyGetStatus(KEY_SHELL) + '0' ;
    DiStr[4] = KeyGetStatus(KEY_POWER) + '0' ;

    while(1)
    {
        key_door  = KeyGetStatus(KEY_DOOR) ;
        key_shell = KeyGetStatus(KEY_SHELL);
        key_power = KeyGetStatus(KEY_POWER);
        key_rsv1  = KeyGetStatus(KEY_RSV1);
        key_rsv2  = KeyGetStatus(KEY_RSV2);
        key_rsv3  = KeyGetStatus(KEY_RSV3);

        //strcpy(&DiStr[0], "DA00000");
        
        if((key_door == DEF_FALSE) && (key_door_prev == DEF_TRUE)) 
        { 
			DiStr[2] = '1';
			key_flag++;
            
            //CamFlag = 1;
			DoorFlag = 1;
			/*
            if(DiStr[2]=='0')
            {
                DiStr[2] = '1';
            }
            else if(DiStr[2]=='1')
            {
                DiStr[2] = '0';
            }
			*/
        }
		else if((key_door == DEF_TRUE) && (key_door_prev == DEF_FALSE))
		{
			DiStr[2] = '0';
			key_flag++;
		}
        
        if((key_shell == DEF_FALSE) && (key_shell_prev == DEF_TRUE))
        {
			DiStr[3] = '1';
            key_flag++;         
        } 
		else if((key_shell == DEF_TRUE) && (key_shell_prev == DEF_FALSE))
        {
			DiStr[3] = '0';
            key_flag++;         
        }
        
       if((key_power == DEF_TRUE) && (key_power_prev == DEF_FALSE)) 
        {
			DiStr[4] = '1';
            key_flag++; 
        }      
        else if((key_power == DEF_FALSE) && (key_power_prev == DEF_TRUE)) 
        {
			DiStr[4] = '0';
            key_flag++; 
        }
  
        if(key_flag!=0)              // 输入变化后发送
        {
            key_flag = 0 ;
            //strcpy(&tx_buf[0], "DA00000");
            //AdToAsc(AdDp, &DiStr[2]);
            CommTxMsg(COMM1, &DiStr[0]);        
        }
        
        //strcpy(&DiStr[0], "DB00000");
        if((key_rsv1 == DEF_FALSE) && (key_rsv1_prev == DEF_TRUE)) 
        {
            DiStr[5] = '0';
        }
        else if((key_rsv2 == DEF_TRUE) && (key_rsv2_prev == DEF_FALSE)) 
        {
            DiStr[6] = '1';
        }
        
        if((key_rsv3 == DEF_FALSE) && (key_rsv3_prev == DEF_TRUE)) 
        {
            DiStr[7] = '0';
        }
        
        key_door_prev  = key_door;
        key_shell_prev = key_shell;
        key_power_prev = key_power;
        key_rsv1_prev  = key_rsv1;
        key_rsv2_prev  = key_rsv2;
        key_rsv3_prev  = key_rsv3;

        OSTimeDlyHMSM(0, 0, 0, 100);
        // msg = 1;
        // OSMboxPost(AltMbox, (void *)&msg);
    }
}

//INT8U  i;

static  void  Rx3Task (void *p_arg)
{ 
    INT8U  c;
	char   cmd;
    INT8U  i;
    INT8U  check;
    INT8U  rstate;
    char   buf[100];
    char   tx_buf[20];
    char   *s;
    char   *pstr;
    INT8U  err;
    
    (void) p_arg;
   
	
	INT8U msg;
	
	msg    = 0;
    
    rstate = 0;
    check  = 0;
    
    //NbInit();
    //CommInit();
	//Uart3Init(9600);
    CommRxIntEn(COMM3);
    while(1) 
    {  
        c = CommGetChar(COMM3, 0, &err);
        //LedOn(LED2);
        
        buf[i] = c;
        i++;
        
        if(rstate==0)
        {
            if(c=='+')
            {
                rstate++;
                s     = &buf[0];
               *s++   = c;
                //check = c;  
                
            }
        }
        else if(rstate==1)
        {
            *s++   = c;
            //check ^= c; 
            if(c==0x0D)
            {
                rstate++;
            }
        }
        else if(rstate==2)
		//else if(rstate==4)
        {
            *s     = c;
            rstate = 0;
            
            i++;
            if(i==5)
            {
               i = 0;
               LedOn(LED_RUN);
            }
			
            //if(check==c)
            {
				s = &buf[0];
                             
                if(strncmp(s, "+CGSN:", 6)==0)		// +CGSN:869976030079405
                {
					s = &buf[6];
					strncpy(nb_imei, s, 15);
					
					msg = NB_S1;
					OSMboxPost(NbMbox, (void *)&msg);
				}
				else if(strncmp(s, "+NCCID:", 7)==0)
				{
					s = &buf[7];
					strncpy(nb_sim, s, 20);
					
					msg = NB_S2;
					OSMboxPost(NbMbox, (void *)&msg);
				}
				else if(strncmp(s, "+CSQ:", 5)==0)
				{
					s = &buf[5];
					strncpy(nb_csq, s, 2);
                    
                    if(nb_csq[1]!=',' && nb_csq[0]<='3')
					{
						if((('1'<=nb_csq[0] && nb_csq[1]>='4')) || ('2'<=nb_csq[0] && nb_csq[0]<='3'))
						{
							csq = (*s - '0')*10;
							s++;
							csq = csq + (*s - '0');
							
							msg = NB_S3;
                            OSMboxPost(NbMbox, (void *)&msg);
						}
					}
				}
				else if(strncmp(s, "+NSORF:", 7)==0)
				{
					s = &buf[31];
					//strncpy(nb_sim, s, 20);
					cmd = buf[47];
					if(cmd=='0')
					{
						NsoFlag = '0';
					}
					else if(cmd=='1')
					{
						NsoFlag = '1';
					}

				}
			}
		}
		
		
		
#ifdef 0
        if(rstate==0)
        {
            if(c==0x02)
            {
                rstate++;
                s     = &buf[0];
               *s++   = c;
                check = c;  
                
            }
        }
        else if(rstate==1)
        {
            *s++   = c;
            check ^= c; 
            if(c==0x03)
            {
                rstate++;
            }
        }
        else if(rstate==2)
        {
            *s = c;
            rstate = 0;
            if(check==c)
            {
                //*(s-1) = 0;  // 0x03转换成0，作为字符串结尾
                pstr = &buf[1];
                             
                if(strncmp(pstr, "DR", 2)==0)
                {
                    s      = &buf[3];
                    //temp   = *s - '0';
                    if(*s=='1')            // OK
                    {
                        //DOOR_SET;
                        strcpy(&buf[0], "DR1OK");
                        CommTxMsg(COMM1, &buf[0]);
                    }
                    else if(*s=='0')            // OK
                    {
                        //DOOR_CLR;
                        strcpy(&buf[0], "DR0OK");
                        CommTxMsg(COMM1, &buf[0]);
                    }
                    /*
                        alt_msg = ALT_STOP;    
                        OSMboxPost(AltMbox, (void *)&alt_msg);
                        
                        spk_msg = SPK_WARN;
                        OSMboxPost(SpkMbox, (void *)&spk_msg);*/
                }
                
                else if(strncmp(pstr, "LA", 2)==0)
                {
                    strcpy(&tx_buf[0], "LA000OK");
                    
                    s      = &buf[3];
                    if(*s=='1')            // OK
                    {
                        LA_R_SET;
                    }
                    else if(*s=='0')      
                    {
                        LA_R_CLR;
                    }
                    tx_buf[2] = *s++;
                    
                    if(*s=='1')            
                    {
                        LA_G_SET;
                    }
                    else if(*s=='0')            
                    {
                        LA_G_CLR;
                    }
                    tx_buf[3] = *s++;
                    
                    if(*s=='1')            
                    {
                        LA_B_SET;
                    }
                    else if(*s=='0')           
                    {
                        LA_B_CLR;
                    }
                    tx_buf[4] = *s;
                    
                    //strcpy(&tx_buf[0], "LA000OK");
                    CommTxMsg(COMM1, &tx_buf[0]);
                        /*
                        alt_msg = ALT_STOP;    
                        OSMboxPost(AltMbox, (void *)&alt_msg);
                        
                        spk_msg = SPK_WARN;
                        OSMboxPost(SpkMbox, (void *)&spk_msg);*/
                }
                
                else if(strncmp(pstr, "LB", 2)==0)
                {
                    strcpy(&tx_buf[0], "LB000OK");
                    
                    s      = &buf[3];
                    if(*s=='1')            // OK
                    {
                        LB_R_SET;
                    }
                    else if(*s=='0')      
                    {
                        LB_R_CLR;
                    }
                    tx_buf[2] = *s++;
                    
                    if(*s=='1')            
                    {
                        LB_G_SET;
                    }
                    else if(*s=='0')            
                    {
                        LB_G_CLR;
                    }
                    tx_buf[3] = *s++;
                    
                    if(*s=='1')            
                    {
                        LB_B_SET;
                    }
                    else if(*s=='0')           
                    {
                        LB_B_CLR;
                    }
                    tx_buf[4] = *s;
                    
                    //strcpy(&tx_buf[0], "LA000OK");
                    CommTxMsg(COMM1, &tx_buf[0]);
                }
                else if(strncmp(pstr, "LC", 2)==0)
                {
                    strcpy(&tx_buf[0], "LC0OK");
                    
                    s      = &buf[3];
                    if(*s=='1')            // OK
                    {
                        LC_SET;
                    }
                    else if(*s=='0')      
                    {
                        LC_CLR;
                    }
                    tx_buf[2] = *s;
                    
                    CommTxMsg(COMM1, &tx_buf[0]);
                }
                
                else if(strncmp(pstr, "LD", 2)==0)
                {
                    strcpy(&tx_buf[0], "LD0OK");
                    
                    s      = &buf[3];
                    if(*s=='1')            
                    {
                        //LD_SET();
                    }
                    else if(*s=='0')      
                    {
                        //LD_CLR();
                    }
                    tx_buf[2] = *s;
                    
                    CommTxMsg(COMM1, &tx_buf[0]);
                }
                  
                else if(strncmp(pstr, "AW", 2)==0)   // 加水回水阀指令 
                {
                    strcpy(&tx_buf[0], "AW00OK");
                    
                    s      = &buf[3];
                    if(*s=='1')                      // 加水阀A控制    
                    {
                        VALVE_A_SET;
                    }
                    else if(*s=='0')      
                    {
                        VALVE_A_CLR;
                    }
                    tx_buf[2] = *s++;
                    
                    OSTimeDlyHMSM(0, 0, 0, 50);                  
                    if(*s=='1')                      // 回水阀B控制
                    {
                        VALVE_B_SET;
                    }
                    else if(*s=='0')      
                    {
                        VALVE_B_CLR;
                    }
                    tx_buf[3] = *s;
                    
                    CommTxMsg(COMM1, &tx_buf[0]);
                }
                
                else if(strncmp(pstr, "DW", 2)==0)    // 排水阀C指令
                {
                    strcpy(&tx_buf[0], "DW0OK");
                    
                    s      = &buf[3];
                    if(*s=='1')            
                    {
                        VALVE_C_SET;
                    }
                    else if(*s=='0')      
                    {
                        VALVE_C_CLR;
                    }
                    tx_buf[2] = *s;
                    
                    CommTxMsg(COMM1, &tx_buf[0]);
                }
                
                else if(strncmp(pstr, "FN", 2)==0)    // 排风扇指令
                {
                    strcpy(&tx_buf[0], "FN0OK");
                    
                    s      = &buf[3];
                    if(*s=='1')            
                    {
                        FAN_SET;
                    }
                    else if(*s=='0')      
                    {
                        FAN_CLR;
                    }
                    tx_buf[2] = *s;
                    
                    CommTxMsg(COMM1, &tx_buf[0]);
                }
                
                else if(strncmp(pstr, "RP", 2)==0)    // 自来水阀指令
                {
                    strcpy(&tx_buf[0], "RP0OK");
                    
                    s      = &buf[3];
                    if(*s=='1')            
                    {
                        VALVE_D_SET;
                    }
                    else if(*s=='0')      
                    {
                        VALVE_D_CLR;
                    }
                    tx_buf[2] = *s;
                    
                    CommTxMsg(COMM1, &tx_buf[0]);
                }
                
                else if(strncmp(pstr, "WP", 2)==0)    // 抽水泵指令
                {
                    strcpy(&tx_buf[0], "WP0OK");
                    
                    s      = &buf[3];
                    if(*s=='1')            
                    {
                        PUMP_A_SET;
                    }
                    else if(*s=='0')      
                    {
                        PUMP_A_CLR;
                    }
                    tx_buf[2] = *s;
                    
                    CommTxMsg(COMM1, &tx_buf[0]);
                }
                
                else if(strncmp(pstr, "DP", 2)==0)    // 排水泵指令
                {
                    strcpy(&tx_buf[0], "DP0OK");
                    
                    s      = &buf[3];
                    if(*s=='1')            
                    {
                        PUMP_B_SET;
                    }
                    else if(*s=='0')      
                    {
                        PUMP_B_CLR;
                    }
                    tx_buf[2] = *s;
                    
                    CommTxMsg(COMM1, &tx_buf[0]);
                }
                
                else if(strncmp(pstr, "OZ", 2)==0)    // 臭氧指令
                {
                    strcpy(&tx_buf[0], "OZ0OK");
                    
                    s      = &buf[3];
                    if(*s=='1')            
                    {
                        OZONE_SET;
                    }
                    else if(*s=='0')      
                    {
                        OZONE_CLR;
                    }
                    tx_buf[2] = *s;
                    
                    CommTxMsg(COMM1, &tx_buf[0]);
                }
                
                else if(strncmp(pstr, "UA", 2)==0)    // 舱内紫外线
                {
                    strcpy(&tx_buf[0], "UA0OK");
                    
                    s      = &buf[3];
                    if(*s=='1')            
                    {
                        UV_A_SET;
                    }
                    else if(*s=='0')      
                    {
                        UV_A_CLR;
                    }
                    tx_buf[2] = *s;
                    
                    CommTxMsg(COMM1, &tx_buf[0]);
                }
                
                else if(strncmp(pstr, "UB", 2)==0)    // 储水紫外线
                {
                    strcpy(&tx_buf[0], "UB0OK");
                    
                    s      = &buf[3];
                    if(*s=='1')            
                    {
                        UV_B_SET;
                    }
                    else if(*s=='0')      
                    {
                        UV_B_CLR;
                    }
                    tx_buf[2] = *s;
                    
                    CommTxMsg(COMM1, &tx_buf[0]);
                }
                
                else if(strncmp(pstr, "HT", 2)==0)    // 加热指令 预留
                {
                    strcpy(&tx_buf[0], "HT0OK");
                    
                    s      = &buf[3];
                    if(*s=='1')            
                    {
                        HEAT_SET;
                    }
                    else if(*s=='0')      
                    {
                        HEAT_CLR;
                    }
                    tx_buf[2] = *s;
                    
                    CommTxMsg(COMM1, &tx_buf[0]);
                }
                
                else if(strncmp(pstr, "OG", 2)==0)    // 氧气指令
                {
                    strcpy(&tx_buf[0], "OG0OK");
                    
                    s      = &buf[3];
                    if(*s=='1')            
                    {
                        O2_SET;
                    }
                    else if(*s=='0')      
                    {
                        O2_CLR;
                    }
                    tx_buf[2] = *s;
                    
                    CommTxMsg(COMM1, &tx_buf[0]);
                }
                
                else if(strncmp(pstr, "AN", 2)==0)    // 负离子指令
                {
                    strcpy(&tx_buf[0], "AN0OK");
                    
                    s      = &buf[3];
                    if(*s=='1')            
                    {
                        ANION_SET;
                    }
                    else if(*s=='0')      
                    {
                        ANION_CLR;
                    }
                    tx_buf[2] = *s;
                    
                    CommTxMsg(COMM1, &tx_buf[0]);
                }
                
                else if(strncmp(pstr, "FL", 2)==0)    // 加香机指令
                {
                    strcpy(&tx_buf[0], "FL0OK");
                    
                    s      = &buf[3];
                    if(*s=='1')            
                    {
                        SCENTED_SET;
                    }
                    else if(*s=='0')      
                    {
                        SCENTED_CLR;
                    }
                    tx_buf[2] = *s;
                    
                    CommTxMsg(COMM1, &tx_buf[0]);
                }
                
                else if(strncmp(pstr, "YT", 2)==0)    // 预留输出指令
                {
                    strcpy(&tx_buf[0], "YT000OK");
                    
                    s      = &buf[3];
                    if(*s=='1')           
                    {
                        Y16_SET;
                    }
                    else if(*s=='0')      
                    {
                        Y16_CLR;
                    }
                    tx_buf[2] = *s++;
                    
                    if(*s=='1')            
                    {
                        Y17_SET;
                    }
                    else if(*s=='0')            
                    {
                        Y17_CLR;
                    }
                    tx_buf[3] = *s++;
                    
                    if(*s=='1')            
                    {
                        //Y23_SET;
                    }
                    else if(*s=='0')           
                    {
                        //Y23_CLR;
                    }
                    tx_buf[4] = *s;
                    
                    CommTxMsg(COMM1, &tx_buf[0]);
                }
                
                else if(strncmp(pstr, "DB", 2)==0)    // 传感器指令
                {
                    //strcpy(&tx_buf[0], "AA0000");                
                    //Ad12BitToAsc(AdTemp, &tx_buf[2]);
                    CommTxMsg(COMM1, &DBStr[0]);     
                }
                
                else if(strncmp(pstr, "AA", 2)==0)    // 温度指令
                {
                    strcpy(&tx_buf[0], "AA0000");                
                    Ad12BitToAsc(ADVal[0], &tx_buf[2]);
                    CommTxMsg(COMM1, &tx_buf[0]);     // 发送温度AD值
                }
                
                else if(strncmp(pstr, "AB", 2)==0)    // 液位指令
                {
                    strcpy(&tx_buf[0], "AB0000");                
                    Ad12BitToAsc(ADVal[1], &tx_buf[2]);
                    CommTxMsg(COMM1, &tx_buf[0]);        // 发送液位AD值
                }
                
                else if(strncmp(pstr, "TS", 2)==0)       // 温度设置指令
                {
                    strcpy(&tx_buf[0], "TS0000OK");                
                    
                    s         = &buf[3];
                    TempSet   = (*s - '0')*1000;         // 设置温度对应AD值千位
                    tx_buf[2] = *s;
                    s++;
                    TempSet = TempSet + (*s - '0')*100;
                    tx_buf[3] = *s;
                    s++;
                    TempSet = TempSet + (*s - '0')*10; 
                    tx_buf[4] = *s;
                    s++;
                    TempSet = TempSet + (*s - '0'); 
                    tx_buf[5] = *s;
                    
                    CommTxMsg(COMM1, &tx_buf[0]);
                }
                
                else if(strncmp(pstr, "VS", 2)==0)        // 读取版本指令
                {
                    strcpy(&tx_buf[0], "VS1801"); 
                    CommTxMsg(COMM1, &tx_buf[0]);                  
                }
                
                else if(strncmp(pstr, "UP", 2)==0)        // 读取版本指令
                {
                    s      = &buf[3];
                    if(*s=='1')            
                    {
                        //SCENTED_SET();  //关闭所有外部输出然后升级？
                    }
                    else if(*s=='0')      
                    {
                        //SCENTED_CLR();
                    }
                    tx_buf[2] = *s;
                    
                    CommTxMsg(COMM1, &tx_buf[0]);                 
                }
                
                else
                {
                    // CommTxMsg(COMM1, "ER2");    // 无此命令
                }
            }
            else
            {
                // CommTxMsg(COMM1, "ER1");        // 命令校验错误
            }
        }
#endif	
    }
}



static  void  Rx1Task (void *p_arg)
{ 
    INT8U  c;
	char   cmd;
    INT8U  i = 0;
    INT8U  check;
    INT8U  rstate;
	INT16U cnt = 0;
    char   buf[40];
   // char   tx_buf[20];
    char   *s;
    char   *pstr;
    INT8U  err;
    
    (void) p_arg;
    
	INT8U  msg;
	
	msg    = 0;
    
    rstate = 0;
    check  = 0;
    
	//Uart1Init(38400);
    CommRxIntEn(COMM1);
    while(1) 
    {  
        c = CommGetChar(COMM1, 0, &err);
        //LedOn(LED2);

        if(rstate==0)
        {
            if(c==0x76)
            {
                rstate++;
                s     = &buf[0];
               *s++   = c;
            }
        }
        else if(rstate==1)
        {
            *s++   = c;
			
            if(c==0x00)
            {
                rstate++;
            }
        }
		else if(rstate==2)
        {
            *s++   = c;
			
            if(c==0x36)
            {
                rstate = 3;
            }
			else if(c==0x34)
            {
                rstate = 4;
            }
			else if(c==0x32)
            {
                rstate = 5;
            }
			else
            {
                rstate = 0;
            }
        }
		
        else if(rstate==3)
        {
            *s++   = c;
			
			i++;
			if(i==2)
			{
				i      = 0;
				rstate = 0;
				
				if(buf[3]==0 && buf[4]==0)
				{
					
				}
			}
		}
		else if(rstate==4)
		{
			*s++   = c;
			
			i++;
			if(i==6)
			{
				i      = 0;
				rstate = 0;
				
				CamLenBuf[0] = buf[7];
				CamLenBuf[1] = buf[8];
				
				CamLen = (CamLenBuf[0] << 8) + CamLenBuf[1];
				
			}
		}
		else if(rstate==5)
		{
			*s++   = c;
			i++;
			
			if(i==2)
			{	
				i      = 0;
				rstate = 6;
				cnt    = 0;
			}
		}
		else if(rstate==6)
		{
			CamBuf[cnt] = c;
			cnt++;
			//if(cnt==CamLen)
			if(LenFlag==0)
			{
				if(cnt==1000+5)
				{
					cnt = 0;
					rstate = 0;
					CamTxFlag = 1;
				}
			}
			else
			{
				if(cnt==len2+5)
				{
					cnt = 0;
					rstate = 0;
					CamTxFlag = 1;
				}
			}

		}
		else 
		{
			i      = 0;
			rstate = 0;
		}
		
    }
}


#if 0
// LED任务
static void SpkTask(void* p_arg)
{
    INT8U  *msg;
  
    INT8U  spk_state;
    INT8U  spk_cnt;
  
    (void) p_arg;
    
    spk_state = 0;
    spk_cnt   = 0;
    SpkVol    = 1;
    
    while(1)
    {
        OSTimeDlyHMSM(0, 0, 0, 100);
        msg = (INT8U *)OSMboxAccept(SpkMbox);
        if(msg != (void *)0)
        {
            if(*msg == SPK_WARN)
            {
                spk_state = 1;
                spk_cnt   = 0;
            }
            else if(*msg == SPK_BEEP)
            {
                spk_state = 0;
                SpkOn(SPK_BEEP, 2);
                OSTimeDlyHMSM(0, 0, 0, 100);
                SpkOff();
            }
            else if(*msg == SPK_NONE)
            {
                spk_state = 0;
                SpkOff();
            }
        }
        else 
        {
            if(spk_state == 1)
            {
                SpkOn(SPK_WARN, SpkVol);
                OSTimeDlyHMSM(0, 0, 0, 300);
                SpkOff();
                OSTimeDlyHMSM(0, 0, 0, 300);
                spk_cnt++;
                if(spk_cnt==20)
                {
                    spk_cnt   = 0;
                    spk_state = 0;
                }
            }
        }
    } 
}
#endif


static void AdTask(void* p_arg)
{
    
    (void) p_arg;
 
    INT8U   i      = 0;
    INT8U   j      = 0;  
    INT16U  sum    = 0;
    INT16U  tmp[ADC_CHAN_NUM];
    INT16U  buf[ADC_CHAN_NUM][10];
    
    INT8U   state     = 0;
    INT16U  time_cnt  = 0;
    
    //INT8U   pump_flag = 0;
    //char   str[10];  
    
    AdcInit();
 
    DMA_Cmd(DMA1_Channel1, ENABLE);          // 启动DMA通道
    ADC_SoftwareStartConvCmd(ADC1, ENABLE);  // 启动ADC转换
  
    while(1)
    {
        // LedTog(LED2);
      
        //LedOff(LED2);
        OSTimeDlyHMSM(0, 0, 0, 100);
        
        for(i=0; i<ADC_CHAN_NUM; i++)
        {

            for(j=0; j<ADC_SPL_NUM; j++)
            {
                sum += ADCBuf[j][i];
            }
            tmp[i] = sum/ADC_SPL_NUM;
            sum    = 0;
        }
        
        for(i=0; i<ADC_CHAN_NUM; i++)
        {
            for(j=9;j>0;j--)
            {
                buf[i][j] = buf[i][j-1]; 
            }
            buf[i][0] = tmp[i];
        }
        
        for(i=0; i<ADC_CHAN_NUM; i++)
        {
            for(j=0; j<10; j++)
            {
                sum += buf[i][j];
            }
            
            ADVal[i] = sum/10;
            sum      = 0;
        } 

		Temp = (V25-ADVal[4])*10/AVG_SLOPE + 15*10;    //放大10倍，取1位小数
		//Temp = (14300-ADVal[4]*33000/4096)/43*10 + 25*10;     //放大10倍，取1位小数
/*
        if(state==0)       //温度控制，液位低于85cm不加热
        {
            if(time_cnt<100)
            {
                time_cnt++;
            }
            if(time_cnt>=100 && ADVal[0]<TempSet && ADVal[1]>AD_LEVEL_MIN)
            {
                state    = 1;
                time_cnt = 0;
            }
        }
        else if(state==1)
        {
            HEAT_SET;
            if(time_cnt<10)
            {
                time_cnt++;
            }
            else if(time_cnt==10)   // 延时1秒开循环泵
            {
                PUMP_C_SET;
            }
            if(ADVal[0]>TempSet+AD_TEMP_DELTA || ADVal[1] < AD_LEVEL_MIN )
            {
                state    = 2;
                time_cnt = 0;
            }
            
        }
        else if(state==2)
        {
            HEAT_CLR;
            if(time_cnt<3000)    // 5分钟间隔加热
            {
                time_cnt++; 
            }  
            if(time_cnt==10)     // 延时1秒关循环泵
            {
                PUMP_C_CLR;
            }
            if(time_cnt>=3000 && ADVal[0]<TempSet-AD_TEMP_DELTA && ADVal[1]>AD_LEVEL_MIN)
            {             
                state    = 1;
                time_cnt = 0;
            }
        }
*/
/*        
        if(AdCh == AD_CH2 && AdCntVal==1)
        {
            strcpy(&str[0], "TR");
            AdToAsc(AdDp, &str[2]);
            CommTxMsg(COMM1, &str[0]);        // 发送差压AD值
        }
        else if(AdCh == AD_CH1 && AdCntVal==1 && AdPsTx == 1)
        {
            strcpy(&str[0], "PR");
            AdToAsc(AdPs, &str[2]);
            CommTxMsg(COMM1, &str[0]);        // 发送测试压AD值
        }
*/       
    } 
}


void CommTxNb(INT8U ch, char *pstr)
{
    INT8U   err;
	INT8U   i;
	INT8U   j;
	INT8U   cyc;
	INT8U   c;
	INT8U   temp;
    INT16U  check;
  
    err = COMM_NO_ERR;
    
	j     = 0;
    cyc   = 24;
	check = 0;
	
	for(i=0; i<4; i++)
	{
		temp   = (NbId>>cyc) & 0xFF;
		cyc    = cyc - 8;	
		check += temp;
		
		for(j=0; j<2; j++)
		{
			if(j==0)
			{
				c = temp>>4;
			}
			else
			{
				c = temp & 0x0F;
			}

			if(c>0x09)
			{
				c = c - 0x0A + 'A';
			}
			else
			{
				c = c + '0';
			}
			err = CommPutChar(ch, c, 0);
		}
	}
	
	while(*pstr && err == COMM_NO_ERR)
    {
        temp   = *pstr++;
		check += temp;
		
		for(j=0; j<2; j++)
		{
			if(j==0)
			{
				c = temp>>4;
			}
			else
			{
				c = temp & 0x0F;
			}

			if(c>0x09)
			{
				c = c - 0x0A + 'A';
			}
			else
			{
				c = c + '0';
			}
			err = CommPutChar(ch, c, 0);
		}
    }
	
	cyc = 8;
	
	for(i=0; i<2; i++)
	{
		temp   = (check>>cyc) & 0xFF;
		cyc    = cyc - 8;	
		check += temp;
		
		for(j=0; j<2; j++)
		{
			if(j==0)
			{
				c = temp>>4;
			}
			else
			{
				c = temp & 0x0F;
			}

			if(c>0x09)
			{
				c = c - 0x0A + 'A';
			}
			else
			{
				c = c + '0';
			}
			err = CommPutChar(ch, c, 0);
		}
		
	}	
}


void CommTxCam(INT8U ch, char *s, INT16U id, INT8U *buf, INT16U len)
{
    INT8U   err;
	INT16U  i;
	INT8U   j;
	INT8U   cyc;
	INT8U   c;
	INT8U   temp;
    INT16U  check;
	
	//INT16U  len;
  
    err = COMM_NO_ERR;
    
	j     = 0;
    cyc   = 24;
	check = 0;
	
	for(i=0; i<4; i++)
	{
		temp   = (NbId>>cyc) & 0xFF;
		cyc    = cyc - 8;	
		check += temp;
		
		for(j=0; j<2; j++)
		{
			if(j==0)
			{
				c = temp>>4;
			}
			else
			{
				c = temp & 0x0F;
			}

			if(c>0x09)
			{
				c = c - 0x0A + 'A';
			}
			else
			{
				c = c + '0';
			}
			err = CommPutChar(ch, c, 0);
		}
	}
	
	while(*s && err == COMM_NO_ERR)
    {
        temp   = *s++;
		check += temp;
		
		for(j=0; j<2; j++)
		{
			if(j==0)
			{
				c = temp>>4;
			}
			else
			{
				c = temp & 0x0F;
			}

			if(c>0x09)
			{
				c = c - 0x0A + 'A';
			}
			else
			{
				c = c + '0';
			}
			err = CommPutChar(ch, c, 0);
		}
    }
    
	cyc = 8;
	
    for(i=0; i<2; i++)
	{
		temp   = (id>>cyc) & 0xFF;
		cyc    = cyc - 8;	
		check += temp;
		
		for(j=0; j<2; j++)
		{
			if(j==0)
			{
				c = temp>>4;
			}
			else
			{
				c = temp & 0x0F;
			}

			if(c>0x09)
			{
				c = c - 0x0A + 'A';
			}
			else
			{
				c = c + '0';
			}
			err = CommPutChar(ch, c, 0);
		}
	}
	
	for(i=0;i<len;i++)
    {
        temp   = buf[i];
		check += temp;
		
		for(j=0; j<2; j++)
		{
			if(j==0)
			{
				c = temp>>4;
			}
			else
			{
				c = temp & 0x0F;
			}

			if(c>0x09)
			{
				c = c - 0x0A + 'A';
			}
			else
			{
				c = c + '0';
			}
			err = CommPutChar(ch, c, 0);
		}
    }
	
	cyc = 8;
	
	for(i=0; i<2; i++)
	{
		temp   = (check>>cyc) & 0xFF;
		cyc    = cyc - 8;	
		check += temp;
		
		for(j=0; j<2; j++)
		{
			if(j==0)
			{
				c = temp>>4;
			}
			else
			{
				c = temp & 0x0F;
			}

			if(c>0x09)
			{
				c = c - 0x0A + 'A';
			}
			else
			{
				c = c + '0';
			}
			err = CommPutChar(ch, c, 0);
		}
		
	}	
}

void CommTxStr(INT8U ch, char *pstr)
{
    INT8U  err;
  
    err = COMM_NO_ERR;
    
    while(*pstr && err == COMM_NO_ERR)
    {
        err = CommPutChar(ch, *pstr++, 0);
    } 
}

void CommTxByte(INT8U ch, INT8U *pstr, INT8U n)
{
    INT8U  i;
	INT8U  err;
  
    err = COMM_NO_ERR;
    
	for(i=0; i<n; i++)
    {
        err = CommPutChar(ch, *pstr++, 0);
    } 
}

void CommTxMsg(INT8U ch, char *pstr)
{
    INT8U  err;
    INT8U  check;
  
    err = COMM_NO_ERR;
    
    check = 0x02;
    
    err = CommPutChar(ch, 0x02, 0);
    while(*pstr && err == COMM_NO_ERR)
    {
        check ^= *pstr;
        err = CommPutChar(ch,*pstr++, 0);
    } 
    check ^= 0x03;
    err = CommPutChar(ch, 0x03, 0);
    err = CommPutChar(ch, check, 0);
}

void delay_us(u32 time)
{    
   u32 i = 0; 
   
   while(time--)
   {
      i = 6;  
      while(i--) ;    
   }
}

void delay_ms(u32 time)
{    
    u32 i = 0; 
	
    while(time--)
    {
       i = 8000;  
       while(i--) ;    
    }
}

void  AdToAsc (INT16U ad_val, char *s)
{
    INT16U  temp;
    
    strcpy(s, "00000");             // Create the template for the selected format
    s[0] = ad_val / 10000 + '0';    // Convert data to ASCII
    temp = ad_val % 10000;
    s[1] = temp   / 1000  + '0';
    temp = temp   % 1000;
    s[2] = temp   / 100   + '0';
    temp = temp   % 100;
    s[3] = temp   / 10    + '0';
    s[4] = temp   % 10    + '0';
}

void  Ad12BitToAsc (INT16U ad_val, char *s)
{
    INT16U  temp;
    
    //strcpy(s, "0000");              // Create the template for the selected format
    s[0] = ad_val / 1000 + '0';     // Convert data to ASCII
    temp = ad_val % 1000;
    s[1] = temp   / 100  + '0';
    temp = temp   % 100;
    s[2] = temp   / 10    + '0';
    s[3] = temp   % 10    + '0';
}

INT16U CommAscToData(char *s)
{
    INT16U temp;
    
    temp = 0;
    
    temp  = ((*s++)-'0')*1000;
    temp += ((*s++)-'0')*100;
    temp += ((*s++)-'0')*10;
    temp += ((*s++)-'0');
    
    return temp;
}

INT32U NbId32(char *s)
{
    INT8U  i;
	INT8U  j;
    INT32U temp1;
    INT32U temp2;
	
	i = 0;
	j = 0;
	
	temp1 = 0;
	temp2 = 0;
    
    while(*s)
    {
        temp1  = temp1 << 8;
		temp1 += *s++;
        //temp1  = temp1 << 8;
        i++;
        if(i==4)
        {
            i = 0;
			if(j==0)
			{
				j++;
				temp2 = temp1;
				temp1 = 0;
			}
			else
			{
				temp2 ^= temp1;
                temp1  = 0;
			}
        }
	}
	
	if(i!=0)
	{
		i = 0;
		temp2 ^= temp1; 
        temp1  = 0;
	}
	
	return temp2;
}


