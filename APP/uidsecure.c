/************************************************************************/
//                     		              
//															              
//                      		                      
//       	              
//                                                                        
/************************************************************************/

#include    "stm32f10x.h"
  
// #define     ALT_SECURE_INTA 

//unsigned int *ChkResult = (unsigned int *)(0x8030000);
unsigned int Fml_Constant = 0x7A9F3D;
unsigned char *C= (unsigned char*)&Fml_Constant;
unsigned char *Uid = (unsigned char *)(0x1FFFF7E8);
unsigned short Fml_CRC16;


//#define	  SERIAL_ADDR             0x08031000    // 序列号地址
//#define	  SECURE_ADDR             0x08030000    // uid加密数据地址
#define	  SERIAL_ADDR             0x0801FC00    // 序列号地址
#define	  SECURE_ADDR             0x0801FD00    // uid加密数据地址



void Formula_16(unsigned char *D,unsigned char *Result)
{
	Result[0] = D[0] - D[1] - D[2] + D[3] - D[4] + D[5] + D[6];
	Result[1] = C[1];
	Result[2] = C[2];
	Result[3] = C[3];
}

void CheckUid(void)
{
unsigned int  serial_id = 0;  
unsigned int  secure_id = 0;
unsigned int  uid       = 0;
unsigned int  uid_buf[3];

#if 0
	unsigned char D[12];
	unsigned int Result;
	D[0] = Uid[0];
	D[1] = Uid[1];
	D[2] = Uid[2];
	D[3] = Uid[3];
	D[4] = Uid[4];
	D[5] = Uid[5];
	D[6] = Uid[6];
	D[7] = Uid[7];
	D[8] = Uid[8];
	D[9] = Uid[9];
	D[10] = Uid[10];
	D[11] = Uid[11];
	Formula_16(D, (unsigned char *)&Result);
	if(Result != *ChkResult)
	{
		while(1);
	}
#endif   
    
    uid_buf[0] = *(unsigned int*)(0x1FFFF7E8);    // 获取UID 后计算加密数据
    uid_buf[1] = *(unsigned int*)(0x1FFFF7EC);
    uid_buf[2] = *(unsigned int*)(0x1FFFF7F0);                     
    uid = (unsigned int)(2*uid_buf[0]+3*uid_buf[1]+uid_buf[2]+0x20151010*3 + 1);

    serial_id = *(unsigned int*)(SERIAL_ADDR);  // 获取32位序列号数据
    if(serial_id==0x20151010)
    {       
        FLASH_Unlock();
        FLASH_ProgramHalfWord( SECURE_ADDR , uid);
        FLASH_ProgramHalfWord( SECURE_ADDR+2 , uid>>16);    // 写入ID号码
        //FLASH_Lock();
    }
    
    secure_id = *(unsigned int*)(SECURE_ADDR);  // 获取32位加密数据
    
    if(secure_id != uid)
    {
        while(1)
        {
            //LedOn(LED2);
            ;
        }
    }
}
