#ifndef APP_CFG_H
#define APP_CFG_H


// task priority 
#define  STARTUP_TASK_PRIO             4
#define  RX3_TASK_PRIO                 5
#define  RX1_TASK_PRIO                 6
#define  NB_TASK_PRIO                  7
#define  AD_TASK_PRIO                  8
#define  CAM_TASK_PRIO                 9
#define  KEY_TASK_PRIO                 10
#define  LED_TASK_PRIO                 11


// task stack size 
#define  STARTUP_TASK_STK_SIZE       256
#define  RX1_TASK_STK_SIZE           256
#define  RX3_TASK_STK_SIZE           256
#define  NB_TASK_STK_SIZE            256
#define  AD_TASK_STK_SIZE            256
#define  CAM_TASK_STK_SIZE           256
#define  KEY_TASK_STK_SIZE           256
#define  LED_TASK_STK_SIZE           256



#define  SPK_NONE                      0
#define  SPK_WARN                      1
#define  SPK_BEEP                      2

#define  CH_NONE                       0
#define  CH_VAL                        1

#define  NB_S0		              0
#define  NB_S1                    1
#define  NB_S2                    2
#define  NB_S3                    3
#define  NB_S4                    4
#define  NB_S5                    5
#define  NB_S6                    6

#define  CAM_S0                   0
#define  CAM_S1                   1
#define  CAM_S2                   2
#define  CAM_S3                   3
#define  CAM_S4                   4
#define  CAM_S5                   5
#define  CAM_S6                   6

#endif
