#ifndef  GENERAL_H
#define  GENERAL_H

#include    <string.h>
#include    "stm32f10x.h"
#include    "stm32f10x_flash.h"
#include    "ucos_ii.h"
#include    "bsp.h"
#include    "commrtos.h"
#include    "uidsecure.h"


#define    ALT_NONE                    0                                            
#define    ALT_START                   1
#define    ALT_STOP                    2
#define    ALT_BUB                     3
#define    ALT_BUB_WARN                4


/*
*********************************************************************************************************
*                                               CONSTANTS
*********************************************************************************************************
*/

#define   MODULE_DIO       1           // Discrete I/O module
#define   MODULE_NB        1           // Discrete I/O module
#define   MODULE_CAM       1           // Discrete I/O module


#if         MODULE_DIO
#include    "dio.h"
#endif

#if         MODULE_DIO
#include    "nb.h"
#endif


#if         MODULE_DIO
#include    "cam.h"
#endif



typedef struct alt_time
{
    INT16U  clamp1;       // 装夹1时间
    INT16U  clamp2;       // 装夹2时间
    INT16U  clamp3;       // 装夹3时间
    INT16U  charge;       // 充气时间
    INT16U  depress;      // 泄压时间
    INT16U  balance1;     // 平衡1时间
    INT16U  balance2;     // 平衡2时间
    INT16U  test;         // 测试时间
    INT16U  blow;         // 吹气时间
    INT16U  exhaust;      // 排气时间
    INT16U  unload1;      // 卸夹1时间
    INT16U  unload2;      // 卸夹2时间
    INT16U  unload3;      // 卸夹3时间

} ALT_TIME;


static  void  StartupTask(void* p_arg);
static  void  AltTask(void* p_arg);
static  void  KeyTask(void* p_arg);
static  void  Rx1Task(void* p_arg);
static  void  Rx3Task(void* p_arg);
static  void  CommTask(void* p_arg);
static  void  AdTask(void* p_arg);
static  void  SpkTask(void* p_arg);
static  void  Led1Task(void* p_arg);
        void  CommTxMsg(INT8U ch, char *pstr);
        void  CommTxStr(INT8U ch, char *pstr);
		void  CommTxByte(INT8U ch, INT8U *pstr, INT8U n);
       
       
INT16U  AltGetAd(INT8U ad_ch);
void    AdToAsc (INT16U ad_val, char *s);
void    Ad12BitToAsc (INT16U ad_val, char *s);

void    delay_us(u32 time);
void    delay_ms(u32 time);
INT16U  CommAscToData(char *s);
void    AltExout(INT16U alt_step);

INT32U  NbId32(char *s);


#endif

