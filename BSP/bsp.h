#ifndef  BSP_H 
#define  BSP_H


#ifdef   BSP_GLOBALS
#define  BSP_EXT
#else
#define  BSP_EXT  extern
#endif

// #include  <cpu.h>
#include  <lib_def.h>
#include "stm32f10x.h" 
#include "ucos_ii.h"



#define    KEY_DOOR           0        // 门传感
#define    KEY_SHELL          1        // 外壳带电
#define    KEY_POWER          2        // 220V有效
#define    KEY_RSV1           3        // 预留1
#define    KEY_RSV2           4        // 预留2
#define    KEY_RSV3           5        // 预留3



#define    LED_RUN          0x00
#define    LED_COMM         0X01
#define    LED_ALL          0xFF

#define    BIT0             0x01
#define    BIT1             0x02
#define    BIT2             0x04
#define    BIT3             0x08
#define    BIT4             0x10
#define    BIT5             0x20
#define    BIT6             0x40
#define    BIT7             0x80

#define    YOUT_START_LAMP   BIT7
#define    YOUT_STOP_LAMP    BIT6
#define    YOUT_BUB_LAMP     BIT5

#define    YOUT_OK_LAMP      BIT0
#define    YOUT_NGP_LAMP     BIT1
#define    YOUT_NGN_LAMP     BIT2

#define    YOUT_MARK_A       BIT7
#define    YOUT_NGP_A        BIT6
#define    YOUT_NGN_A        BIT5
#define    YOUT_OK_A         BIT4
#define    YOUT_MARK_B       BIT3
#define    YOUT_NGP_B        BIT2
#define    YOUT_NGN_B        BIT1
#define    YOUT_OK_B         BIT0

#define    AD_CH_NONE          0
#define    AD_CH1              1
#define    AD_CH2              2


#define  VALVE_A_SET             GPIO_SetBits  (GPIOA, GPIO_Pin_4)    // 加水阀Y00
#define  VALVE_A_CLR             GPIO_ResetBits(GPIOA, GPIO_Pin_4)    

#define  VALVE_B_SET             GPIO_SetBits  (GPIOA, GPIO_Pin_5)    // 回水阀Y01  
#define  VALVE_B_CLR             GPIO_ResetBits(GPIOA, GPIO_Pin_5)

#define  VALVE_C_SET             GPIO_SetBits  (GPIOA, GPIO_Pin_6)    // 排水阀Y02
#define  VALVE_C_CLR             GPIO_ResetBits(GPIOA, GPIO_Pin_6)
                                    
#define  PUMP_A_SET              GPIO_SetBits  (GPIOA, GPIO_Pin_7)    // 抽水阀Y03
#define  PUMP_A_CLR              GPIO_ResetBits(GPIOA, GPIO_Pin_7)

#define  PUMP_B_SET              GPIO_SetBits  (GPIOC, GPIO_Pin_4)    // 排水泵Y04
#define  PUMP_B_CLR              GPIO_ResetBits(GPIOC, GPIO_Pin_4)

#define  PUMP_C_SET              GPIO_SetBits  (GPIOC, GPIO_Pin_5)    // 循环泵Y05
#define  PUMP_C_CLR              GPIO_ResetBits(GPIOC, GPIO_Pin_5)

#define  OZONE_SET               GPIO_SetBits  (GPIOB, GPIO_Pin_0)    // 臭氧Y06
#define  OZONE_CLR               GPIO_ResetBits(GPIOB, GPIO_Pin_0)

#define  UV_A_SET                GPIO_SetBits  (GPIOB, GPIO_Pin_1)    // 舱内紫外线Y07
#define  UV_A_CLR                GPIO_ResetBits(GPIOB, GPIO_Pin_1)
 
#define  FAN_SET                 GPIO_SetBits  (GPIOB, GPIO_Pin_2)    // 排风扇Y10
#define  FAN_CLR                 GPIO_ResetBits(GPIOB, GPIO_Pin_2)

#define  VALVE_D_SET             GPIO_SetBits  (GPIOE, GPIO_Pin_7)    // 排风扇Y11
#define  VALVE_D_CLR             GPIO_ResetBits(GPIOE, GPIO_Pin_7)

#define  UV_B_SET                GPIO_SetBits  (GPIOE, GPIO_Pin_8)    // 储水罐紫外线Y12
#define  UV_B_CLR                GPIO_ResetBits(GPIOE, GPIO_Pin_8)

#define  LC_SET                  GPIO_SetBits  (GPIOE, GPIO_Pin_9)    // 投影灯Y13
#define  LC_CLR                  GPIO_ResetBits(GPIOE, GPIO_Pin_9)

#define  HEAT_SET                GPIO_SetBits  (GPIOE, GPIO_Pin_10)   // 加热Y14
#define  HEAT_CLR                GPIO_ResetBits(GPIOE, GPIO_Pin_10)

#define  O2_SET                  GPIO_SetBits  (GPIOE, GPIO_Pin_11)   // 氧气Y15
#define  O2_CLR                  GPIO_ResetBits(GPIOE, GPIO_Pin_11)

#define  Y16_SET                 GPIO_SetBits  (GPIOE, GPIO_Pin_12)   // 预留输出Y16
#define  Y16_CLR                 GPIO_ResetBits(GPIOE, GPIO_Pin_12)

#define  Y17_SET                 GPIO_SetBits  (GPIOE, GPIO_Pin_13)   // 预留输出Y17
#define  Y17_CLR                 GPIO_ResetBits(GPIOE, GPIO_Pin_13)

#define  ANION_SET               GPIO_SetBits  (GPIOE, GPIO_Pin_14)   // 负离子Y20
#define  ANION_CLR               GPIO_ResetBits(GPIOE, GPIO_Pin_14)

#define  SCENTED_SET             GPIO_SetBits  (GPIOE, GPIO_Pin_15)   // 加香机Y21
#define  SCENTED_CLR             GPIO_ResetBits(GPIOE, GPIO_Pin_15)

#define  LA_R_SET                GPIO_SetBits  (GPIOB, GPIO_Pin_12)   // 环境灯R输出Y24
#define  LA_R_CLR                GPIO_ResetBits(GPIOB, GPIO_Pin_12)

#define  LA_G_SET                GPIO_SetBits  (GPIOB, GPIO_Pin_13)   // 环境灯G输出Y25
#define  LA_G_CLR                GPIO_ResetBits(GPIOB, GPIO_Pin_13)

#define  LA_B_SET                GPIO_SetBits  (GPIOB, GPIO_Pin_14)   // 环境灯B输出Y26
#define  LA_B_CLR                GPIO_ResetBits(GPIOB, GPIO_Pin_14)

#define  LB_R_SET                GPIO_SetBits  (GPIOB, GPIO_Pin_15)   // 舱底灯R输出Y27
#define  LB_R_CLR                GPIO_ResetBits(GPIOB, GPIO_Pin_15)

#define  LB_G_SET                GPIO_SetBits  (GPIOD, GPIO_Pin_8)    // 舱底灯G输出Y30
#define  LB_G_CLR                GPIO_ResetBits(GPIOD, GPIO_Pin_8)

#define  LB_B_SET                GPIO_SetBits  (GPIOD, GPIO_Pin_9)    // 舱底灯B输出Y31
#define  LB_B_CLR                GPIO_ResetBits(GPIOD, GPIO_Pin_9) 


BSP_EXT INT8U YoutGrpData[4];

#define ADC_SPL_NUM    10   // 每通道采10次
#define ADC_CHAN_NUM    6   // 为6个通道

//const INT16U V25       = 0x06E2;		// 温度为 25 摄氏度时的电压值 
//const INT16U AVG_SLOPE = 0x0005;		//温度、电压对应的的斜率每摄氏度4.35mV对应每摄氏度0x05
#define V25         0x06E2		// 温度为 25 摄氏度时的电压值 
#define AVG_SLOPE   0x0005		// 温度、电压对应的的斜率每摄氏度4.35mV对应每摄氏度0x05

BSP_EXT INT16S  Temp;
BSP_EXT INT16U  ADCBuf[ADC_SPL_NUM][ADC_CHAN_NUM];   // 用来存放ADC转换结果，也是DMA的目标地址
//BSP_EXT INT16U  After_filter[ADC_NUM];             // 用来存放求平均值之后的结果

void    systick_init(void); //函数声明
void    BspInit(void);
void    NbInit(void);

void  Uart1Init(INT16U baud);
void  Uart2Init(INT16U baud);
void  Uart3Init(INT16U baud);

//void    YoutOn  (INT8U grp_id, INT8U yout_id);
//void    YoutOff (INT8U grp_id, INT8U yout_id);

void    LedOn  (INT8U led);
void    LedOff (INT8U led);
void    LedTog (INT8U led);

void    Delay(INT8U i);
void    delay_us(u32 time);
void    delay_ms(u32 time);

INT8U   CommCfgPort (INT8U ch, INT16U baud, INT8U bits, INT8U parity, INT8U stops);
void    CommRxIntEn (INT8U ch);

BOOLEAN KeyGetStatus (INT8U  key_id);
void    Ad7705Init(void);
INT16U  Ad7705GetVal(INT8U ad_ch);
INT16U  Ad7705RdReg(INT8U ad_ch);

INT16U   Ad7705Rd(void);
void     Ad7705Wr(INT8U wr_data);
void     Ad7705SelCh(INT8U ad_ch);

INT8U    ChGetEn(void);
INT8U    ChGetCh(void);

void     SpkOn(INT8U spk_type, INT8U spk_vol);
void     SpkOff(void);

void     AdcInit(void);

void     FlashRdProtectInit();
void     FlashRdProtectEnable();
void     FlashRdProtectDisable();



 
#endif
