/*
*********************************************************************************************************
*                                     BOARD SUPPORT PACKAGE
*
*                             (c) Copyright 2008; XXXX, Inc.
*
*               All rights reserved.  Protected by international copyright laws.
*               Knowledge of the source code may NOT be used to develop a similar product.
*               Please help us continue to provide the Embedded community with the finest
*               software available.  Your honesty is greatly appreciated.
*********************************************************************************************************
*/

/*
*********************************************************************************************************
*
*                                        BOARD SUPPORT PACKAGE
*
*                                     ST Microelectronics STM32
*                                              on the 
*                                            FC18 Board
*
* Filename      : bsp.c
* Version       : V1.00
* Programmer(s) : CH
*********************************************************************************************************
*/

/*
*********************************************************************************************************
*                                             INCLUDE FILES
*********************************************************************************************************
*/

#define  BSP_GLOBALS
#include <bsp.h>
#include <commrtos.h>

extern INT8U   AdCh;
extern INT32U  AdTemp;
extern INT16U  AdCnt;
extern INT8U   AdDpCnt;
extern INT8U   AdPsCnt;
extern INT8U   AdCntVal;



/*
*********************************************************************************************************
*                                            LOCAL DEFINES
*********************************************************************************************************
*/

#define  GPIOB_LED_RUN                         GPIO_Pin_6             // PB6-LED_RUN
#define  GPIOB_LED_COMM                        GPIO_Pin_5             // PB5-LED_COMM

#define  NB_SET()                    			GPIO_SetBits  (GPIOA, GPIO_Pin_7)
#define  NB_CLR()                    			GPIO_ResetBits(GPIOA, GPIO_Pin_7)


/*
#define  BSP_GPIOB_KEY_DOOR              GPIO_Pin_7
#define  BSP_GPIOB_KEY_LIGHT             GPIO_Pin_6
#define  BSP_GPIOB_KEY_MUSIC             GPIO_Pin_5
#define  BSP_GPIOD_KEY_RSV1              GPIO_Pin_6
#define  BSP_GPIOD_KEY_RSV2              GPIO_Pin_5
*/

#define  AD_RST_SET()                    GPIO_SetBits  (GPIOA, GPIO_Pin_4)
#define  AD_RST_CLR()                    GPIO_ResetBits(GPIOA, GPIO_Pin_4)
#define  AD_SCLK_SET()                   GPIO_SetBits  (GPIOA, GPIO_Pin_5)
#define  AD_SCLK_CLR()                   GPIO_ResetBits(GPIOA, GPIO_Pin_5)
#define  AD_DIN_SET()                    GPIO_SetBits  (GPIOA, GPIO_Pin_7)
#define  AD_DIN_CLR()                    GPIO_ResetBits(GPIOA, GPIO_Pin_7)
#define  AD_DOUT_RD()                    GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_6)
#define  AD_DRDY_RD()                    GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_4)


     


/*
*********************************************************************************************************
*                                           EXTERNAL FUNCTIONS
*********************************************************************************************************
*/

extern  void  SCP1000_IntrHook (void);

/*
*********************************************************************************************************
*                                          LOCAL DATA TYPES
*********************************************************************************************************
*/


/*
*********************************************************************************************************
*                                            LOCAL TABLES
*********************************************************************************************************
*/


/*
*********************************************************************************************************
*                                       LOCAL GLOBAL VARIABLES
*********************************************************************************************************
*/


/*
*********************************************************************************************************
*                                      LOCAL FUNCTION PROTOTYPES
*********************************************************************************************************
*/

//static  void  YoutOe    (INT8U oe);

static  void  YoutInit(void);
static  void  LedInit (void);
static  void  XinInit (void);
static  void  UartInit(void);
//static  void  AdcInit   (void);
static  void  SpkInit   (void);
static  void  AdTmr2Init(void);



void  BspInit(void)
{
    SystemInit();              // 72M时钟
    systick_init();            // 嘀嗒时钟配置
   
	//CommInit();
    LedInit();
	XinInit();
	YoutInit();
    
    NbInit();
    CommInit();
	Uart3Init(9600);
	//CommRxIntEn(COMM3);
	
	Uart1Init(38400);
	
    //UartInit();
	//NbInit();
    
    //AdcInit();
//  SpkInit();
//    AdTmr2Init();
//    FlashRdProtectInit(); 
}

void systick_init(void)
{
    RCC_ClocksTypeDef rcc_clocks;
    
    RCC_GetClocksFreq(&rcc_clocks);
    SysTick_Config(rcc_clocks.HCLK_Frequency/OS_TICKS_PER_SEC);
}


void SpkInit(void)
{
    GPIO_InitTypeDef         GPIO_InitStructure;
    TIM_TimeBaseInitTypeDef  TIM_BaseInitStructure; 
    TIM_OCInitTypeDef        TIM_OCInitStructure; 

    RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
    
    GPIO_InitStructure.GPIO_Pin    = GPIO_Pin_5;
    GPIO_InitStructure.GPIO_Speed  = GPIO_Speed_50MHz;  
    GPIO_InitStructure.GPIO_Mode   = GPIO_Mode_AF_PP; 
    // GPIO_InitStructure.GPIO_Mode   = GPIO_Mode_Out_PP; 
    GPIO_Init(GPIOB, &GPIO_InitStructure);              // PB5 set as SPK
    
    GPIO_PinRemapConfig(GPIO_PartialRemap_TIM3, ENABLE);
    
    /*
    while(1)
    {
        GPIO_SetBits(GPIOB, GPIO_Pin_5);
        delay_us(800);
        GPIO_ResetBits(GPIOB, GPIO_Pin_5);
        delay_us(800);
        
    }*/
    
    TIM_BaseInitStructure.TIM_Prescaler = 4-1;        // 72M/4=8M时钟
    TIM_BaseInitStructure.TIM_Period    = 8000-1;     // 8M/8000=1K频率   8000做报警音 10000做报提示   
    TIM_BaseInitStructure.TIM_ClockDivision = 0; 
    TIM_BaseInitStructure.TIM_CounterMode = TIM_CounterMode_Up; 
    TIM_BaseInitStructure.TIM_RepetitionCounter = 0; 
    TIM_TimeBaseInit(TIM3, &TIM_BaseInitStructure);

    TIM_ARRPreloadConfig(TIM3, ENABLE);      //启用ARR的影子寄存器（直到产生更新事件才更改设置）
    
    TIM_OCInitStructure.TIM_OCMode      = TIM_OCMode_PWM2;       // TIM1_OC1模块设置（设置1通道占空比） 
    TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable; 
    TIM_OCInitStructure.TIM_OCPolarity  = TIM_OCPolarity_Low;    // TIM_OCInitStructure.TIM_OutputNState = TIM_OutputNState_Enable; 
    //TIM_OCInitStructure.TIM_Pulse       = 360;                   // TIM_OCInitStructure.TIM_OCNPolarity = TIM_OCNPolarity_High; 
    TIM_OCInitStructure.TIM_Pulse       = 0;
    TIM_OC2Init(TIM3, &TIM_OCInitStructure);
    
    TIM_OC2PreloadConfig(TIM3, TIM_OCPreload_Enable);
    
    TIM_Cmd(TIM3, ENABLE);             //TIM3开启 
    TIM_CtrlPWMOutputs(TIM3, ENABLE);

    // TIM_SetCompare2(TIM3, 3000);
    /*
    TIM_SetCompare2(TIM3, 8000);  // 40000做报警音
    TIM_SetCompare2(TIM3, 25000);
    */

    TIM_SetCompare2(TIM3, 0);
}

/*
void SpkSetVol(INT8U spk_vol)
{
    switch (spk_vol) 
    {
        case SPK_WARN:
            TIM_BaseInitStructure.TIM_Period = 50000-1;
            break;
        case SPK_BEEP:
            TIM_BaseInitStructure.TIM_Period = 60000-1;
            break;
        default:
             break;
    }
}
*/

void SpkOn(INT8U spk_type, INT8U spk_vol)
{
    TIM_TimeBaseInitTypeDef  TIM_BaseInitStructure;
    
    TIM_BaseInitStructure.TIM_Prescaler = 4-1;        // 72M/4=8M时钟
    TIM_BaseInitStructure.TIM_Period    = 8000-1;    // 8M/8000=1K频率   8000做报警音 10000做报提示    
    TIM_BaseInitStructure.TIM_ClockDivision = 0; 
    TIM_BaseInitStructure.TIM_CounterMode = TIM_CounterMode_Up; 
    TIM_BaseInitStructure.TIM_RepetitionCounter = 0; 
    // TIM_TimeBaseInit(TIM3, &TIM_BaseInitStructure);
    
    switch (spk_type) 
    {
        case SPK_WARN:
            TIM_BaseInitStructure.TIM_Period = 8000-1;
            break;
        case SPK_BEEP:
            TIM_BaseInitStructure.TIM_Period = 10000-1;
            break;
        default:
             break;
    }
    
    TIM_TimeBaseInit(TIM3, &TIM_BaseInitStructure);
    
    switch (spk_vol) 
    {
        case 0:
            TIM_SetCompare2(TIM3, 0);
            break;
            
        case 1:
            TIM_SetCompare2(TIM3, 9000/30);
            break;
            
        case 2:
            TIM_SetCompare2(TIM3, 9000/15);
            break;
            
        case 3:
            TIM_SetCompare2(TIM3, 9000/8);
            break;
        default:
             break;
    }
    
    
}

void SpkOff(void)
{
    TIM_SetCompare2(TIM3, 0);
}



/*
************************************************************************************************************
************************************************************************************************************
**                                             YOUT SERVICES
************************************************************************************************************
************************************************************************************************************
*/

/*
*********************************************************************************************************
*                                         YoutInit()
*
* Description : Initializes the board's LEDs.
*
* Argument(s) : none
*
* Return(s)   : none.
*
* Caller(s)   : BSP_Init().
*
* Note(s)     : none.
*********************************************************************************************************
*/

void  YoutInit (void)
{   
    GPIO_InitTypeDef  GPIO_InitStructure;

    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
    
    GPIO_InitStructure.GPIO_Pin    = GPIO_Pin_6;   				// PA6 -> relay control
    GPIO_InitStructure.GPIO_Speed  = GPIO_Speed_50MHz;  
    GPIO_InitStructure.GPIO_Mode   = GPIO_Mode_Out_PP;  
    GPIO_Init(GPIOA, &GPIO_InitStructure);    
    
}

void  NbInit (void)
{   
    GPIO_InitTypeDef  GPIO_InitStructure;

    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
    
    GPIO_InitStructure.GPIO_Pin    = GPIO_Pin_7;
    GPIO_InitStructure.GPIO_Speed  = GPIO_Speed_50MHz;  
    GPIO_InitStructure.GPIO_Mode   = GPIO_Mode_Out_PP;  
    GPIO_Init(GPIOA, &GPIO_InitStructure);  
    
	NB_SET();
    delay_ms(100);
	NB_CLR();
	delay_ms(1000);
    
}





/*
*********************************************************************************************************
*                                             BspLedOn()
*
* Description : Turns on the LEDs on the board specified by led parameter.
*
* Argument(s) : led  is the number of the LED to control
*                       0    indicates that you want ALL the LEDs to be ON
*                       1    turns ON user LED1  on the board
*                       2    turns ON user LED2  on the board
*
* Return(s)  : none.
*
* Caller(s)  : Application.
*
* Note(s)    : none.
*********************************************************************************************************
*/

void  YoutOn (INT8U grp_id, INT8U yout_id)
{
    INT16U  reg_val; 
    
    reg_val  =  GPIO_ReadOutputData(GPIOD);
    reg_val &=  0xFF00;
  
    switch (grp_id) {
  
        case 0x00:
             YoutGrpData[0] |= yout_id;
             reg_val +=  YoutGrpData[0];
             GPIO_Write(GPIOD, reg_val);
             //YoutGrpLatch(YOUT_GRP_A);
             break;
          
        case 0x01:
             YoutGrpData[1] |= yout_id;
             reg_val +=  YoutGrpData[1];
             GPIO_Write(GPIOD, reg_val);
             //YoutGrpLatch(YOUT_GRP_B);
             break;
             
        case 0x02:
             YoutGrpData[2] |= yout_id;
             reg_val +=  YoutGrpData[2];
             GPIO_Write(GPIOD, reg_val);
             //YoutGrpLatch(YOUT_GRP_C);
             break;
             
        case 0x03:
             YoutGrpData[3] |= yout_id;
             reg_val +=  YoutGrpData[3];
             GPIO_Write(GPIOD, reg_val);
             //YoutGrpLatch(YOUT_GRP_D);
             break;
             
        case 0xFF:
             reg_val |=  0x00FF;
             GPIO_Write(GPIOD, reg_val);
             //YoutGrpLatch(YOUT_GRP_ALL);
             break;
        default:
             break;
    }
}

/*
*********************************************************************************************************
*                                             YoutOff()
*
* Description : Turns off the LEDs on the board specified by led parameter.
*
* Argument(s) : led  is the number of the LED to turn OFF
*                       0    indicates that you want ALL the out to be OFF
*                       1    turns OFF user LED1  on the board
*                       2    turns OFF user LED2  on the board
*
* Return(s)  : none
*
* Caller(s)  : Application.
*
* Note(s)    : none.

*********************************************************************************************************
*/

void  YoutOff (INT8U grp_id, INT8U yout_id)
{
    INT16U  reg_val; 
    
    reg_val  =  GPIO_ReadOutputData(GPIOD);
    reg_val &=  0xFF00;
  
    switch (grp_id) {
  
        case 0x00:
             YoutGrpData[0] &=  ~yout_id;
             reg_val += YoutGrpData[0];
             GPIO_Write(GPIOD, reg_val);
             //YoutGrpLatch(YOUT_GRP_A);
             break;
          
        case 0x01:
             YoutGrpData[1] &=  ~yout_id;
             reg_val += YoutGrpData[1];
             GPIO_Write(GPIOD, reg_val);
             //YoutGrpLatch(YOUT_GRP_B);
             break;
             
        case 0x02:
             YoutGrpData[2] &=  ~yout_id;
             reg_val += YoutGrpData[2];
             GPIO_Write(GPIOD, reg_val);
             //YoutGrpLatch(YOUT_GRP_C);
             break;
             
        case 0x03:
             YoutGrpData[3] &=  ~yout_id;
             reg_val += YoutGrpData[3];
             GPIO_Write(GPIOD, reg_val);
             //YoutGrpLatch(YOUT_GRP_D);
             break;
         
        case 0xFF:
             // reg_val &=  0xFF00;
             GPIO_Write(GPIOD, reg_val);
             //YoutGrpLatch(YOUT_GRP_ALL);
             break;

        default:
             break;
    }
}


/*
************************************************************************************************************
************************************************************************************************************
**                                             LED SERVICES
************************************************************************************************************
************************************************************************************************************
*/

/*
*********************************************************************************************************
*                                         BSP_LED_Init()
*
* Description : Initializes the board's LEDs.
*
* Argument(s) : none
*
* Return(s)   : none.
*
* Caller(s)   : BSP_Init().
*
* Note(s)     : none.
*********************************************************************************************************
*/

static  void  LedInit (void)
{   
    GPIO_InitTypeDef  GPIO_InitStructure;

    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
    
    GPIO_InitStructure.GPIO_Pin    = GPIO_Pin_5 | GPIO_Pin_6; 
    GPIO_InitStructure.GPIO_Speed  = GPIO_Speed_50MHz;  
    GPIO_InitStructure.GPIO_Mode   = GPIO_Mode_Out_PP;  
    GPIO_Init(GPIOB, &GPIO_InitStructure); 
}

/*
*********************************************************************************************************
*                                             LedOn()
*
* Description : Turns on the LEDs on the board specified by led parameter.
*
* Argument(s) : led  is the number of the LED to control
*                       0    indicates that you want ALL the LEDs to be ON
*                       1    turns ON user LED1  on the board
*                       2    turns ON user LED2  on the board
*
* Return(s)  : none.
*
* Caller(s)  : Application.
*
* Note(s)    : none.
*********************************************************************************************************
*/

void  LedOn (INT8U led)
{
    switch (led) {

        case 0x00:
             GPIO_SetBits(GPIOB, GPIOB_LED_RUN);
             break;

        case 0x01:
             GPIO_SetBits(GPIOB, GPIOB_LED_COMM);
             break;
             
        case 0xFF:
             GPIO_SetBits(GPIOB, GPIOB_LED_RUN);
             GPIO_SetBits(GPIOB, GPIOB_LED_COMM);
             break;

        default:
             break;
    }
}

/*
*********************************************************************************************************
*                                             LedOff()
*
* Description : Turns off the LEDs on the board specified by led parameter.
*
* Argument(s) : led  is the number of the LED to turn OFF
*                       0    indicates that you want ALL the LEDs to be OFF
*                       1    turns OFF user LED1  on the board
*                       2    turns OFF user LED2  on the board
*
* Return(s)  : none
*
* Caller(s)  : Application.
*
* Note(s)    : none.

*********************************************************************************************************
*/

void  LedOff (INT8U led)
{
    switch (led) {
        case 0x00:
             GPIO_ResetBits(GPIOB, GPIOB_LED_RUN);
             break;

        case 0x01:
             GPIO_ResetBits(GPIOB, GPIOB_LED_COMM);
             break;
         
        case 0xFF:
             GPIO_ResetBits(GPIOB, GPIOB_LED_RUN);
             GPIO_ResetBits(GPIOB, GPIOB_LED_COMM);
             break;

        default:
             break;
    }
}

/*
*********************************************************************************************************
*                                             BSP_LED_Toggle()
*
* Description : Turns off the LEDs on the board specified by led parameter.
*
* Argument(s) : led  is the number of the LED to toggle.
*                       0    indicates that you want ALL the LEDs to be toggled
*                       1    toggles user LED1  on the board
*                       2    toggles user LED2  on the board
*
* Return(s)   : none.
*
* Caller(s)   : Application.
*
* Note(s)     : none.
*********************************************************************************************************
*/

void  LedTog (INT8U led)
{
    INT16U  temp;


    switch (led) {
        case 0:
             temp  =  GPIO_ReadOutputData(GPIOB);
             temp ^=  GPIOB_LED_RUN ;
             GPIO_Write(GPIOB, temp);
             break;

        case 1:
             temp  =  GPIO_ReadOutputData(GPIOB);
             temp ^=  GPIOB_LED_COMM;
             GPIO_Write(GPIOB, temp);
             break;

        case 0xFF:
             temp  =  GPIO_ReadOutputData(GPIOB);
             temp ^=  GPIOB_LED_RUN | GPIOB_LED_COMM;
             GPIO_Write(GPIOB, temp);
             break;
        default:
             break;
    }
}


/*
************************************************************************************************************
************************************************************************************************************
**                                             Xin SERVICES
************************************************************************************************************
************************************************************************************************************
*/

/*
*********************************************************************************************************
*                                         XinInit()
*
* Description : Initializes the board's INPUT.
*
* Argument(s) : none
*
* Return(s)   : none.
*
* Caller(s)   : BSP_Init().
*
* Note(s)     : none.
*********************************************************************************************************
*/

void  XinInit (void)
{   
    GPIO_InitTypeDef  GPIO_InitStructure;

    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);   
    
    //RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);  
    //GPIO_PinRemapConfig(GPIO_Remap_SWJ_JTAGDisable, ENABLE);    //  PB3 作为GPIO
    
    GPIO_InitStructure.GPIO_Pin    =  GPIO_Pin_4  | GPIO_Pin_5  | GPIO_Pin_8; // PA4 PA5 PA8 set as input
    GPIO_InitStructure.GPIO_Speed  =  GPIO_Speed_50MHz;  
    GPIO_InitStructure.GPIO_Mode   =  GPIO_Mode_IPD;  
    GPIO_Init(GPIOA, &GPIO_InitStructure);                       
    
}

/*
INT8U  ChGetEn(void)
{
    return (GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_6));    // PC6 set as CH ENABLE
}

INT8U  ChGetCh(void)
{
    INT16U ch;
    
    ch = GPIO_ReadInputData(GPIOD);   // PD10--PD15 set as CH0--CH5
    ch = ch>> 10;
    
    return ((INT8U)ch);
}
*/

void FlashRdProtectInit()
{
    GPIO_InitTypeDef  GPIO_InitStructure;
    
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
  
    GPIO_InitStructure.GPIO_Pin    = GPIO_Pin_0;
    GPIO_InitStructure.GPIO_Speed  = GPIO_Speed_50MHz;  
    GPIO_InitStructure.GPIO_Mode   = GPIO_Mode_IPD;  
    GPIO_Init(GPIOB, &GPIO_InitStructure);                      // PB0 set as flash lock switch
}

void FlashRdProtectEnable()
{
    if(FLASH_GetReadOutProtectionStatus() != SET)
    {
        FLASH_Unlock();
        FLASH_ReadOutProtection(ENABLE);   
    }
}

void FlashRdProtectDisable()
{
    if(GPIO_ReadInputDataBit(GPIOB, GPIO_Pin_0)!=0)
    {
        if(FLASH_GetReadOutProtectionStatus() != RESET)
        {
            FLASH_Unlock();
            FLASH_ReadOutProtection(DISABLE);
        }
    }
}

/*
************************************************************************************************************
************************************************************************************************************
**                                             PB SERVICES
************************************************************************************************************
************************************************************************************************************
*/

/*
*********************************************************************************************************
*                                         BSP_PB_GetStatus()
*
* Description : Gets the status of any push button on the board.
*
* Argument(s) : pb_id       is the number of the push button to probe
*                           1    probe the PB1 'USER'   push button
*                           2    probe the PB2 'WKUP'   push button
*
* Return(s)   : The current status of the push button.
*                           DEF_TRUE    if the push button is pressed.
*                           DEF_FALSE   if the push button is not pressed.
*
* Caller(s)   : Application.
*
* Note(s)     : none.
*********************************************************************************************************
*/

BOOLEAN  KeyGetStatus (INT8U  key_id)
{
    BOOLEAN  status;
    INT8U    reg_val;


    status = DEF_FALSE;    
    
    switch (key_id) {
        case KEY_DOOR:
             reg_val = GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_8);
             if (reg_val == 1) {
               status = DEF_TRUE;
             }
             break;

        case KEY_SHELL:
             reg_val = GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_5);
             if (reg_val == 1) {
               status = DEF_TRUE;
             }
             break;

        case KEY_POWER:
             reg_val = GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_4);
             if (reg_val == 1) {
               status = DEF_TRUE;
             }
             break;
             
        case KEY_RSV1:
             reg_val = GPIO_ReadInputDataBit(GPIOB, GPIO_Pin_13);
             if (reg_val == 1) {
               status = DEF_TRUE;
             }
             break;       
             
        case KEY_RSV2:
             reg_val = GPIO_ReadInputDataBit(GPIOB, GPIO_Pin_14);
             if (reg_val == 1) {
               status = DEF_TRUE;
             }
             break;    
             
        case KEY_RSV3:
             reg_val = GPIO_ReadInputDataBit(GPIOD, GPIO_Pin_15);
             if (reg_val == 1) {
               status = DEF_TRUE;
             }
             break; 
            
        default:
             break;
    }

    return (status);
}


/*
*********************************************************************************************************
*                                            CONFIGURE PORT
*
* Description : This function is used to configure a serial I/O port.  This code is for IBM-PCs and
*               compatibles and assumes a National Semiconductor NS16450.
*
* Arguments   : 'ch'          is the COMM port channel number and can either be:
*                                 COMM1
*                                 COMM2
*               'baud'        is the desired baud rate (anything, standard rates or not)
*               'bits'        defines the number of bits used and can be either 5, 6, 7 or 8.
*               'parity'      specifies the 'parity' to use:
*                                 COMM_PARITY_NONE
*                                 COMM_PARITY_ODD
*                                 COMM_PARITY_EVEN
*               'stops'       defines the number of stop bits used and can be either 1 or 2.
*
* Returns     : COMM_NO_ERR   if the channel has been configured.
*               COMM_BAD_CH   if you have specified an incorrect channel.
*
* Notes       : 1) Refer to the NS16450 Data sheet
*               2) The constant 115200 is based on a 1.8432 MHz crystal oscillator and a 16 x Clock.
*               3) 'lcr' is the Line Control Register and is define as:
*
*                        B7  B6  B5  B4  B3  B2  B1  B0
*                                                ------ #Bits  (00 = 5, 01 = 6, 10 = 7 and 11 = 8)
*                                            --         #Stops (0 = 1 stop, 1 = 2 stops)
*                                        --             Parity enable (1 = parity is enabled)
*                                    --                 Even parity when set to 1.
*                                --                     Stick parity (see 16450 data sheet)
*                            --                         Break control (force break when 1)
*                        --                             Divisor access bit (set to 1 to access divisor)
*               4) This function enables Rx interrupts but not Tx interrupts.
*********************************************************************************************************
*/


static void UartInit()
{
    GPIO_InitTypeDef  GPIO_InitStructure;
    USART_InitTypeDef USART_InitStructure;
	NVIC_InitTypeDef  NVIC_InitStructure;
    
	//RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_AFIO, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);
    
    // Uart1
    GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_9;         		// PA9-U1TX
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_AF_PP;
    GPIO_Init(GPIOA, &GPIO_InitStructure); 
    
    GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_10;         		// PA10-U1RX
    GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_IN_FLOATING;
    GPIO_Init(GPIOA, &GPIO_InitStructure);
	
	USART_InitStructure.USART_BaudRate            = 38400;
    USART_InitStructure.USART_WordLength          = USART_WordLength_8b;
    USART_InitStructure.USART_StopBits            = USART_StopBits_1;
    USART_InitStructure.USART_Parity              = USART_Parity_No;
    USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
    USART_InitStructure.USART_Mode                = USART_Mode_Rx | USART_Mode_Tx;
    USART_Init(USART1, &USART_InitStructure);

	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_0);
    NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
	
	USART_ITConfig(USART1, USART_IT_RXNE, ENABLE); 
	//USART_ITConfig(USART1, USART_IT_TXE, ENABLE);
	USART_Cmd(USART1,  ENABLE);

    //GPIO_PinRemapConfig(GPIO_Remap_USART1, ENABLE);
/*    
    // Uart2
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA , ENABLE);
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE);
    
    GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_2;         // PA2-U2TX
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_AF_PP;
    GPIO_Init(GPIOA, &GPIO_InitStructure);

    GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_3;         // PA3-U2RX
    GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_IN_FLOATING;
    GPIO_Init(GPIOA, &GPIO_InitStructure);
*/  
	// Uart3
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
    RCC_APB2PeriphClockCmd(RCC_APB1Periph_USART3, ENABLE);
	
	GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_10;         		// PB10-U3TX
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_AF_PP;
    GPIO_Init(GPIOB, &GPIO_InitStructure); 
    
    GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_11;         		// PB11-U3RX
    GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_IN_FLOATING;
    GPIO_Init(GPIOB, &GPIO_InitStructure);   
    
    USART_InitStructure.USART_BaudRate            = 115200;
    USART_InitStructure.USART_WordLength          = USART_WordLength_8b;
    USART_InitStructure.USART_StopBits            = USART_StopBits_1;
    USART_InitStructure.USART_Parity              = USART_Parity_No;
    USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
    USART_InitStructure.USART_Mode                = USART_Mode_Rx | USART_Mode_Tx;
    USART_Init(USART3, &USART_InitStructure);
	
	USART_ITConfig(USART3, USART_IT_RXNE, ENABLE);
	//USART_ITConfig(USART3, USART_IT_TXE, ENABLE);
	USART_Cmd(USART3, ENABLE);
  
    
    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_0);
    NVIC_InitStructure.NVIC_IRQChannel = USART3_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 2;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
}

void Uart1Init(INT16U baud)
{
    GPIO_InitTypeDef  GPIO_InitStructure;
    USART_InitTypeDef USART_InitStructure;
	NVIC_InitTypeDef  NVIC_InitStructure;
    
    //RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);
    
    GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_9;         		// PA9-U1TX
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_AF_PP;
    GPIO_Init(GPIOA, &GPIO_InitStructure); 
    
    GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_10;         		// PA10-U1RX
    GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_IN_FLOATING;
    GPIO_Init(GPIOA, &GPIO_InitStructure);
	
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_0);
    NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
	
	USART_InitStructure.USART_BaudRate            = baud;		// 38400
    USART_InitStructure.USART_WordLength          = USART_WordLength_8b;
    USART_InitStructure.USART_StopBits            = USART_StopBits_1;
    USART_InitStructure.USART_Parity              = USART_Parity_No;
    USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
    USART_InitStructure.USART_Mode                = USART_Mode_Rx | USART_Mode_Tx;
    USART_Init(USART1, &USART_InitStructure);

	
	USART_ITConfig(USART1, USART_IT_RXNE, ENABLE); 
	//USART_ITConfig(USART1, USART_IT_TXE, ENABLE);
	USART_Cmd(USART1,  ENABLE);

}

static void Uart2Init(INT16U baud)
{
    GPIO_InitTypeDef  GPIO_InitStructure;
    USART_InitTypeDef USART_InitStructure;
	NVIC_InitTypeDef  NVIC_InitStructure;
	
    //RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
    RCC_APB2PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE);
	
	GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_2;         					// PA2-U2TX
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_AF_PP;
    GPIO_Init(GPIOA, &GPIO_InitStructure); 
    
    GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_3;         					// PA3-U2RX
    GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_IN_FLOATING;
    GPIO_Init(GPIOA, &GPIO_InitStructure); 
	
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_0);
    NVIC_InitStructure.NVIC_IRQChannel = USART2_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 2;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
	
	USART_InitStructure.USART_BaudRate            = baud;					// 9600
    USART_InitStructure.USART_WordLength          = USART_WordLength_8b;
    USART_InitStructure.USART_StopBits            = USART_StopBits_1;
    USART_InitStructure.USART_Parity              = USART_Parity_No;
    USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
    USART_InitStructure.USART_Mode                = USART_Mode_Rx | USART_Mode_Tx;
    USART_Init(USART2, &USART_InitStructure);
	
	USART_ITConfig(USART2, USART_IT_RXNE, ENABLE);
	//USART_ITConfig(USART2, USART_IT_TXE, ENABLE);
	USART_Cmd(USART2, ENABLE);
}

void Uart3Init(INT16U baud)
{
    GPIO_InitTypeDef  GPIO_InitStructure;
    USART_InitTypeDef USART_InitStructure;
	NVIC_InitTypeDef  NVIC_InitStructure;
	
    //RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART3, ENABLE);
	
	GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_10;         					// PB10-U3TX
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_AF_PP;
    GPIO_Init(GPIOB, &GPIO_InitStructure); 
    
    GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_11;         					// PB11-U3RX
    GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_IN_FLOATING;
    GPIO_Init(GPIOB, &GPIO_InitStructure); 
	
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_0);
    NVIC_InitStructure.NVIC_IRQChannel = USART3_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 6;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
	
	USART_InitStructure.USART_BaudRate            = baud;					// 115200
    USART_InitStructure.USART_WordLength          = USART_WordLength_8b;
    USART_InitStructure.USART_StopBits            = USART_StopBits_1;
    USART_InitStructure.USART_Parity              = USART_Parity_No;
    USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
    USART_InitStructure.USART_Mode                = USART_Mode_Rx | USART_Mode_Tx;
    USART_Init(USART3, &USART_InitStructure);
	
	//USART_ITConfig(USART3, USART_IT_RXNE, ENABLE);
	//USART_ITConfig(USART3, USART_IT_TXE, ENABLE);
	USART_Cmd(USART3, ENABLE);
}



INT8U  CommCfgPort (INT8U ch, INT16U baud, INT8U bits, INT8U parity, INT8U stops)
{

#if 0
    INT16U  div;                                      /* Baud rate divisor                             */
    INT8U   divlo;
    INT8U   divhi;
    INT8U   lcr;                                      /* Line Control Register                         */
    INT16U  base;                                     /* COMM port base address                        */

    switch (ch) {                                     /* Obtain base address of COMM port              */
        case COMM1:
             base = COMM1_BASE;
             break;

        case COMM2:
             base = COMM2_BASE;
             break;

        default:
             return (COMM_BAD_CH);
    }
    div   = (INT16U)(115200L / (INT32U)baud);         /* Compute divisor for desired baud rate         */
    divlo =  div & 0x00FF;                            /* Split divisor into LOW and HIGH bytes         */
    divhi = (div >> 8) & 0x00FF;
    lcr   = ((stops - 1) << 2) + (bits - 5);
    switch (parity) {
        case COMM_PARITY_ODD: 
             lcr |= 0x08;                             /* Odd  parity                                   */
             break;

        case COMM_PARITY_EVEN: 
             lcr |= 0x18;                             /* Even parity                                   */
             break;
    }
    OS_ENTER_CRITICAL();
    outp(base + COMM_UART_LCR, BIT7);                 /* Set divisor access bit                        */
    outp(base + COMM_UART_DIV_LO, divlo);             /* Load divisor                                  */
    outp(base + COMM_UART_DIV_HI, divhi);
    outp(base + COMM_UART_LCR, lcr);                  /* Set line control register (Bit 8 is 0)        */
    outp(base + COMM_UART_MCR, BIT3 | BIT1 | BIT0);   /* Assert DTR and RTS and, allow interrupts      */
    outp(base + COMM_UART_IER, 0x00);                 /* Disable both Rx and Tx interrupts             */
    OS_EXIT_CRITICAL();
    // CommRxFlush(ch);                                  /* Flush the Rx input                            */
#endif
    
    UartInit();
    return (COMM_NO_ERR);
}


/*$PAGE*/
/*
*********************************************************************************************************
*                                          ENABLE RX INTERRUPTS
*
* Description : This function enables the Rx interrupt.
* Arguments   : 'ch'    is the COMM port channel number and can either be:
*                           COMM1
*                           COMM2
*********************************************************************************************************
*/

void  CommRxIntEn (INT8U ch)
{
  
#if 0
    INT8U stat;
    
    switch (ch) {
        case COMM1:
             OS_ENTER_CRITICAL();
                                                           /* Enable Rx interrupts                     */
             stat = (INT8U)inp(COMM1_BASE + COMM_UART_IER) | BIT0;     
             outp(COMM1_BASE + COMM_UART_IER, stat);
                                                           /* Enable IRQ4 on the PC                    */
             outp(PIC_MSK_REG_PORT, (INT8U)inp(PIC_MSK_REG_PORT) & ~BIT4);     
             OS_EXIT_CRITICAL();
             break;

        case COMM2:
             OS_ENTER_CRITICAL();
                                                           /* Enable Rx interrupts                     */
             stat = (INT8U)inp(COMM2_BASE + COMM_UART_IER) | BIT0;     
             outp(COMM2_BASE + COMM_UART_IER, stat);
                                                           /* Enable IRQ3 on the PC                    */
             outp(PIC_MSK_REG_PORT, (INT8U)inp(PIC_MSK_REG_PORT) & ~BIT3);     
             OS_EXIT_CRITICAL();
             break;
    }
#endif
    
    #if OS_CRITICAL_METHOD == 3                      // Allocate storage for CPU status register  
    OS_CPU_SR  cpu_sr = 0;
    #endif
    switch (ch) {
        case COMM1:
             OS_ENTER_CRITICAL();
             USART_ITConfig(USART1, USART_IT_RXNE, ENABLE); 
             OS_EXIT_CRITICAL();
             break;
             
        case COMM2:
             OS_ENTER_CRITICAL();
             USART_ITConfig(USART2, USART_IT_RXNE, ENABLE);
             OS_EXIT_CRITICAL();
             break;
             
        case COMM3:
             OS_ENTER_CRITICAL();
             USART_ITConfig(USART3, USART_IT_RXNE, ENABLE);
             OS_EXIT_CRITICAL();
             break;
    }
    
}

/*$PAGE*/
/*
*********************************************************************************************************
*                                          ENABLE TX INTERRUPTS
*
* Description : This function enables transmission of characters.  Transmission of characters is
*               interrupt driven.  If you are using a multi-drop driver, the code must enable the driver
*               for transmission.
* Arguments   : 'ch'    is the COMM port channel number and can either be:
*                           COMM1
*                           COMM2
*                           COMM3
*********************************************************************************************************
*/

void  CommTxIntEn (INT8U ch)
{
  
    #if OS_CRITICAL_METHOD == 3                      // Allocate storage for CPU status register  
    OS_CPU_SR  cpu_sr = 0;
    #endif
    
    switch (ch) {
        case COMM1:
             OS_ENTER_CRITICAL();
             USART_ITConfig(USART1, USART_IT_TXE, ENABLE); 
             OS_EXIT_CRITICAL();
             break;
        case COMM2:
             OS_ENTER_CRITICAL();
             USART_ITConfig(USART2, USART_IT_TXE, ENABLE);
             OS_EXIT_CRITICAL();
             break;
        case COMM3:
             OS_ENTER_CRITICAL();
             USART_ITConfig(USART3, USART_IT_TXE, ENABLE);
             OS_EXIT_CRITICAL();
             break;
    }
}



/*$PAGE*/
/*
*********************************************************************************************************
*                                          ADC
*
* Description : This function enables transmission of characters.  Transmission of characters is
*               interrupt driven.  If you are using a multi-drop driver, the code must enable the driver
*               for transmission.
* Arguments   : 'ch'    is the ADC channel channel number and can either be:
*                           AD_CH1
*                           AD_CH2
*********************************************************************************************************
*/

void AdcInit()
{
    GPIO_InitTypeDef GPIO_InitStructure;
    
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE); 
    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE);
    GPIO_InitStructure.GPIO_Pin  = GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3;  // PC0-PC3 作为模拟输入
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AIN; 
    GPIO_Init(GPIOC, &GPIO_InitStructure);
    
    
    DMA_InitTypeDef DMA_InitStructure;                              // 设置DMA
    
    DMA_DeInit(DMA1_Channel1);                                      // 将DMA的通道1寄存器重设为缺省值
    DMA_InitStructure.DMA_PeripheralBaseAddr = (u32)&ADC1->DR;      // DMA外设ADC基地址
    DMA_InitStructure.DMA_MemoryBaseAddr = (u32)&ADCBuf;            // DMA内存基地址
    DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralSRC;              // 内存作为数据传输的目的地
    DMA_InitStructure.DMA_BufferSize = ADC_SPL_NUM *ADC_CHAN_NUM;   // DMA通道的DMA缓存的大小
    DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;  // 外设地址寄存器不变
    DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;           // 内存地址寄存器递增
    DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;  // 数据宽度为16位
    DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_HalfWord;          // 数据宽度为16位
    DMA_InitStructure.DMA_Mode = DMA_Mode_Circular;        // 工作在循环缓存模式
    DMA_InitStructure.DMA_Priority = DMA_Priority_High;    // DMA通道 x拥有高优先级
    DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;           // DMA通道x没有设置为内存到内存传输
    DMA_Init(DMA1_Channel1, &DMA_InitStructure);           // 根据DMA_InitStruct中指定的参数初始化DMA的通道
 
    
    ADC_InitTypeDef ADC_InitStructure;

    ADC_DeInit(ADC1);                                      // 将外设 ADC1 的全部寄存器重设为缺省值

    ADC_InitStructure.ADC_Mode = ADC_Mode_Independent;     // ADC工作模式:ADC1和ADC2工作在独立模式
    ADC_InitStructure.ADC_ScanConvMode =ENABLE;            // 模数转换工作在扫描模式
    ADC_InitStructure.ADC_ContinuousConvMode = ENABLE;     // 模数转换工作在连续转换模式
    ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_None; // 外部触发转换关闭
    ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;              // ADC数据右对齐
    ADC_InitStructure.ADC_NbrOfChannel = 6;                             // 顺序进行规则转换的ADC通道的数目
    ADC_Init(ADC1, &ADC_InitStructure);                                 // 根据ADC_InitStruct中指定的参数初始化外设ADCx的寄存器

    RCC_ADCCLKConfig(RCC_PCLK2_Div8);    // 配置ADC时钟，为PCLK2的8分频，即9M
    //设置指定ADC的规则组通道，设置它们的转化顺序和采样时间
    //ADC1,ADC通道x,规则采样顺序值为y,采样时间为239.5周期
    ADC_RegularChannelConfig(ADC1, ADC_Channel_10, 1, ADC_SampleTime_239Cycles5 );
    ADC_RegularChannelConfig(ADC1, ADC_Channel_11, 2, ADC_SampleTime_239Cycles5 );
    ADC_RegularChannelConfig(ADC1, ADC_Channel_12, 3, ADC_SampleTime_239Cycles5 );
    ADC_RegularChannelConfig(ADC1, ADC_Channel_13, 4, ADC_SampleTime_239Cycles5 ); 
    ADC_RegularChannelConfig(ADC1, ADC_Channel_TempSensor, 5, ADC_SampleTime_239Cycles5 );
    ADC_RegularChannelConfig(ADC1, ADC_Channel_Vrefint,    6, ADC_SampleTime_239Cycles5 );
    
    ADC_TempSensorVrefintCmd(ENABLE);  // 开启内部温度传感器

    ADC_DMACmd(ADC1, ENABLE);          // 开启ADC的DMA支持（要实现DMA功能，还需独立配置DMA通道等参数）

    ADC_Cmd(ADC1, ENABLE);             // 使能指定的ADC1

    ADC_ResetCalibration(ADC1);        // 复位指定的ADC1的校准寄存器

    while(ADC_GetResetCalibrationStatus(ADC1)); // 获取ADC1复位校准寄存器的状态,设置状态则等待

    ADC_StartCalibration(ADC1);                 // 开始指定ADC1的校准状态

    while(ADC_GetCalibrationStatus(ADC1));      // 获取指定ADC1的校准程序,设置状态则等待
    
    //ADC_SoftwareStartConvCmd(ADC1, ENABLE);   // 软件触发ADC转换 
}


void Delay(INT8U i)
{
    INT16U j; 
    for(j=0; j<i; j++)
    {
        j = j;
    }
}

// 写AD的控制程序
void Ad7705Wr(INT8U wr_data)    
{
    INT8U i = 0;

    // AD_SCLK_SET();
    
    for(i=0; i<8; i++)
    {
        if((wr_data & 0x80)==0x80)
        {
            AD_DIN_SET();
        }
        else
        {
            AD_DIN_CLR();
        }
        
        Delay(5);
        AD_SCLK_CLR();
        Delay(5);
        AD_SCLK_SET();
        wr_data = wr_data<<1;
    }
 }


/*
函数名:     Ad7705Rd(void)
函数功能:   读ad转换的数据，返回一个无符号的16进制数
返回数据：  data   
*/

INT16U Ad7705Rd(void)
{
    INT8U    i    = 0;
    INT16U   data = 0;
    
    // AD_SCLK_SET();
    for(i=0; i<16; i++)
    {
        data = data<<1;
        Delay(5);
        AD_SCLK_CLR();
        Delay(5);
        AD_SCLK_SET();
        if(AD_DOUT_RD()==1)
        {
            data = data | 0x0001;
        }
    }
    
    return(data);
}


void Ad7705SelCh(INT8U ad_ch)
{
    AdTemp   = 0;        // 清除缓存及计数器
    AdCnt    = 0;
    AdDpCnt  = 0;
    AdPsCnt  = 0;
    AdCntVal = 0;
    
    AdCh     = ad_ch;
}


void Ad7705Init(void)
{
    INT8U i;
    
    GPIO_InitTypeDef  GPIO_InitStructure;
    EXTI_InitTypeDef  EXTI_InitStructure;
    NVIC_InitTypeDef  NVIC_InitStructure; 

    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOC, ENABLE);
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);
    
    GPIO_InitStructure.GPIO_Pin    = GPIO_Pin_4 | GPIO_Pin_5 | GPIO_Pin_7;
    GPIO_InitStructure.GPIO_Speed  = GPIO_Speed_50MHz;  
    GPIO_InitStructure.GPIO_Mode   = GPIO_Mode_Out_PP;  
    GPIO_Init(GPIOA, &GPIO_InitStructure);                      // PA4-RSTn PA5-SCLK PA7-DIN  
   
    GPIO_InitStructure.GPIO_Pin    = GPIO_Pin_6;
    GPIO_InitStructure.GPIO_Speed  = GPIO_Speed_50MHz;  
    GPIO_InitStructure.GPIO_Mode   = GPIO_Mode_IPU;  
    GPIO_Init(GPIOA, &GPIO_InitStructure);                      // PA6-DOUT
    
    GPIO_InitStructure.GPIO_Pin    = GPIO_Pin_4;
    GPIO_InitStructure.GPIO_Speed  = GPIO_Speed_50MHz;  
    // GPIO_InitStructure.GPIO_Mode   = GPIO_Mode_IN_FLOATING; 
    GPIO_InitStructure.GPIO_Mode   = GPIO_Mode_IPU;
    GPIO_Init(GPIOC, &GPIO_InitStructure);                      // PC4-DRDYn
    
    GPIO_EXTILineConfig(GPIO_PortSourceGPIOC, GPIO_PinSource4);  //将PC4连接到外部中断通道
    
    EXTI_InitStructure.EXTI_Line = EXTI_Line4;                  // 外部中断通道4
    EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
    EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;     // 下降降沿触发
    EXTI_InitStructure.EXTI_LineCmd = ENABLE;                   //使能
    EXTI_Init(&EXTI_InitStructure);
    
    
    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_0);
    
    NVIC_InitStructure.NVIC_IRQChannel = EXTI4_IRQn;          // 指定中断源
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 3;        // 指定响应优先级别1
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;           // 使能外部中断通道3
    NVIC_Init(&NVIC_InitStructure);
    
    AD_RST_CLR();
    delay_ms(10);
    AD_RST_SET();
    delay_ms(1);
    
    // cs=0;                 // 片选始终给0
    AD_SCLK_SET();
    AD_DIN_SET();
    
    for(i=0;i<40;i++)     // 若接口序列丢失，在至少32个时钟后，回到正常
    {
        Delay(5);
        AD_SCLK_CLR();
        Delay(5);
        AD_SCLK_SET();
    }

    Ad7705Wr(0x20);      // 选择时钟寄存器，通道1                                             
    Ad7705Wr(0x0B);      // 2MHz时钟，200Hz数据更新速率
    Ad7705Wr(0x10);      // 选择设置寄存器，写，正常工作模式，选择通道1
    Ad7705Wr(0x44);      // 自检模式，增益为1，单端模式，缓冲器使能，调制器、滤波器开始工作
    delay_ms(200);  

    Ad7705Wr(0x21);      // 选择时钟寄存器，写	                                             
    Ad7705Wr(0x0B);      // 2MHz时钟，200Hz数据更新速率
    Ad7705Wr(0x11) ;     // 选择设置寄存器，写，正常工作模式，选择通道2 
    Ad7705Wr(0x44);      // 自检模式，增益为1，单端模式，缓冲器使能，调制器、滤波器开始工作
    delay_ms(200);


}



//  定时时间T计算公式：           
//  T=(TIM_Period+1)*(TIM_Prescaler+1)/TIMxCLK=(35999+1)*(1999+1)/72MHz=1s 

void AdTmr2Init(void) 
{
    
    NVIC_InitTypeDef NVIC_InitStructure;                       // 定义结构体变量 
    
    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_0);            // 0组，全副优先级           
    NVIC_InitStructure.NVIC_IRQChannel=TIM2_IRQn;              // 选择中断通道，库P166页，     
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;  // 抢占优先级0  
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 4;         // 响应优先级0    
    NVIC_InitStructure.NVIC_IRQChannelCmd=ENABLE;              // 启动此通道的中断   
    NVIC_Init(&NVIC_InitStructure);                            // 结构体初始化
    
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2,ENABLE);        // 使能TIM2外设时钟
    
    TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;             // 定义TIM结构体变量 
    
    TIM_DeInit(TIM2);                                          // 复位时钟TIM2，恢复到初始状态  
    // TIM_TimeBaseStructure.TIM_Period=17999;                 // 35999和1999刚好1s  
    TIM_TimeBaseStructure.TIM_Period    = 359;
    TIM_TimeBaseStructure.TIM_Prescaler = 1999;               
    TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1;    // TIM2时钟分频   
    TIM_TimeBaseStructure.TIM_CounterMode=TIM_CounterMode_Up;  // 计数方式  
    TIM_TimeBaseInit(TIM2, &TIM_TimeBaseStructure);            // 初始化 
    
    TIM_ClearFlag(TIM2, TIM_FLAG_Update);                      // 清除标志  
    TIM_ITConfig(TIM2, TIM_IT_Update,ENABLE);                  // 中断方式下，使能中断源  
    TIM_Cmd(TIM2,ENABLE);                                      // 使能TIM2
   
}



