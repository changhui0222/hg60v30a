/**
  ******************************************************************************
  * @file    Project/STM32F10x_StdPeriph_Template/stm32f10x_it.c 
  * @author  MCD Application Team
  * @version V3.5.0
  * @date    08-April-2011
  * @brief   Main Interrupt Service Routines.
  *          This file provides template for all exceptions handler and 
  *          peripherals interrupt service routine.
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2011 STMicroelectronics</center></h2>
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "stm32f10x_it.h"
#include "bsp.h"
#include "ucos_ii.h"
#include "commrtos.h"
#include <General.h>

//extern  INT16U      AdDp;
extern  INT16U      AdPs;
extern  INT8U       AdCh;

INT16U      AdDpBuf[16];
INT16U      AdPsBuf[16];

extern  INT8U     AdCntVal;
extern  INT16U    AdCnt;
extern  INT8U     AdDpCnt;
extern  INT8U     AdPsCnt;
extern  INT8U     AdDpState;
extern  INT16U    AdDpTmr;
extern  INT16U    AdDpA;
extern  INT16U    AdDpB;

extern  ALT_TIME    alt_time;

/** @addtogroup STM32F10x_StdPeriph_Template
  * @{
  */

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

/******************************************************************************/
/*            Cortex-M3 Processor Exceptions Handlers                         */
/******************************************************************************/

/**
  * @brief  This function handles NMI exception.
  * @param  None
  * @retval None
  */

void USART1_IRQHandler(void)
{
    INT8U      c;
    INT8U      err;
    OS_CPU_SR  cpu_sr;
    
    OS_ENTER_CRITICAL();   // Tell uC/OS-II that we are starting an ISR
    OSIntNesting++;
    OS_EXIT_CRITICAL();
    
    if(USART_GetITStatus(USART1, USART_IT_RXNE) != RESET)      // 中断产生 
    {   
        USART_ClearITPendingBit(USART1,USART_IT_RXNE);         // 清除中断标志 
        c = USART_ReceiveData(USART1);
        CommPutRxChar(COMM1, c);  
    }

    if(USART_GetITStatus(USART1, USART_IT_TXE) != RESET)     // 发送中断
    {  
        c = CommGetTxChar(COMM1, &err);          // Get next character to send.        
        if (err == COMM_TX_EMPTY)                // Do we have anymore characters to send ? 
        {               
            USART_ITConfig(USART1, USART_IT_TXE, DISABLE);  // No,  Disable Tx interrupts  
        } 
        else 
        {
            USART_SendData(USART1, c);           // Yes, Send character                     
        }
    }    
    OSIntExit();
}

void USART3_IRQHandler(void)
{
    INT8U      c;
    INT8U      err;
    OS_CPU_SR  cpu_sr;
    
    OS_ENTER_CRITICAL();   // Tell uC/OS-II that we are starting an ISR
    OSIntNesting++;
    OS_EXIT_CRITICAL();
    
    if(USART_GetITStatus(USART3, USART_IT_RXNE) != RESET)      // 中断产生 
    {   
        USART_ClearITPendingBit(USART3, USART_IT_RXNE);         // 清除中断标志 
        c = USART_ReceiveData(USART3);
        CommPutRxChar(COMM3, c);  
    }

    if(USART_GetITStatus(USART3, USART_IT_TXE) != RESET)     // 发送中断
    {  
        c = CommGetTxChar(COMM3, &err);          // Get next character to send.        
        if (err == COMM_TX_EMPTY)                // Do we have anymore characters to send ? 
        {               
            USART_ITConfig(USART3, USART_IT_TXE, DISABLE);  // No,  Disable Tx interrupts  
        } 
        else 
        {
            USART_SendData(USART3, c);           // Yes, Send character                     
        }
    }    
    OSIntExit();
}


INT16U  AdBuf[20];
//INT32U  AdTemp  = 0;

void EXTI4_IRQHandler(void)  
{
    OS_CPU_SR  cpu_sr;
     
    OS_ENTER_CRITICAL();   // Tell uC/OS-II that we are starting an ISR
    OSIntNesting++;
    OS_EXIT_CRITICAL();
    
    if(EXTI_GetITStatus(EXTI_Line4) != RESET)
    {
        EXTI_ClearFlag(EXTI_Line4);          // 清除中断标志
        EXTI_ClearITPendingBit(EXTI_Line4);
/*       
        if(AdCh == AD_CH2)
        {
            // AdDp = Ad7705Rd();
            // AdTemp += Ad7705Rd();
            Ad7705Wr(0x39);
            AdBuf[AdCnt] = Ad7705Rd();
            AdCnt++;
            if(AdCnt == 6)
            {
                AdCnt    = 0;
                AdCntVal = 1;
                
                for(j=0;j<6-1;j++)
                {
                    for(i=0;i<6-1-j;i++)
                    {
                        if(AdBuf[i]>AdBuf[i+1])    // 数组元素大小按升序排列
                        {
                            temp       = AdBuf[i];
                            AdBuf[i]   = AdBuf[i+1];
                            AdBuf[i+1] = temp;
                        }
                    }
                }
                
                //AdTemp = AdBuf[2] + AdBuf[3] + AdBuf[4]+ AdBuf[5] + AdBuf[6] + AdBuf[7];
                // AdDp = AdTemp/6;
                AdTemp = AdBuf[1] + AdBuf[2]+ AdBuf[3] + AdBuf[4];
                
                for(i=15;i>0;i--)
                {
                    AdDpBuf[i] = AdDpBuf[i-1]; 
                }
                
                AdDpBuf[0] = AdTemp/4;
                
                if(AdDpCnt<16)
                {
                    AdDpCnt++;
                    AdDp    = AdDpBuf[0];
                    AdTemp  = 0;
                }
                else
                {  
                    for(i=0;i<16;i++)
                    {
                        AdBuf[i] = AdDpBuf[i];
                    }  
                  
                    for(j=0;j<16-1;j++)
                    {
                        for(i=0;i<16-1-j;i++)
                        {
                            if(AdBuf[i]>AdBuf[i+1])    // 数组元素大小按升序排列
                            {
                                temp       = AdBuf[i];
                                AdBuf[i]   = AdBuf[i+1];
                                AdBuf[i+1] = temp;
                            }
                        }
                    }
                   //  AdTemp =  AdBuf[6]+ AdBuf[7] + AdBuf[8]+ AdBuf[9]+ AdBuf[10]+ AdBuf[11] + AdBuf[12]+ AdBuf[13];
                    AdTemp =  AdBuf[5]+ AdBuf[6] + AdBuf[7]+ AdBuf[8]+ AdBuf[9] + AdBuf[10];
                    AdDp   = AdTemp/6;
                    AdTemp = 0;
                }
            }
            
            //// AdCh = AD_CH1;
            //// Ad7705Wr(0x39);
        }
#if 0        
        else if(AdCh == AD_CH1)
        {          
            Ad7705Wr(0x38);
            AdBuf[AdCnt] = Ad7705Rd();
            AdCnt++;
            if(AdCnt == 6)
            {
                AdCnt    = 0;
                AdCntVal = 1;
                
                for(i=0;i<6-1;i++)
                {
                    for(j=0;j<6-1-i;j++)
                    {
                        if(AdBuf[j]>AdBuf[j+1])//数组元素大小按升序排列
                        {
                            temp       = AdBuf[j];
                            AdBuf[j]   = AdBuf[j+1];
                            AdBuf[j+1] = temp;
                        }
                    }
                }
                
                // AdTemp = AdBuf[2] + AdBuf[3] + AdBuf[4]+ AdBuf[5] + AdBuf[6] + AdBuf[7];
                // AdPs = AdTemp/6;
                AdTemp = AdBuf[1] + AdBuf[2]+ AdBuf[3] + AdBuf[4];
                AdPs   = AdTemp/4;
                AdTemp = 0;
            }
            
            //// Ad7705Wr(0x38);
            // AdCh = AD_CH2;
        } 
#endif
        else if(AdCh == AD_CH1)
        {
            Ad7705Wr(0x38);
            AdBuf[AdCnt] = Ad7705Rd();
            AdCnt++;
            if(AdCnt == 6)
            {
                AdCnt    = 0;
                AdCntVal = 1;
                
                for(j=0;j<6-1;j++)
                {
                    for(i=0;i<6-1-j;i++)
                    {
                        if(AdBuf[i]>AdBuf[i+1])    // 数组元素大小按升序排列
                        {
                            temp       = AdBuf[i];
                            AdBuf[i]   = AdBuf[i+1];
                            AdBuf[i+1] = temp;
                        }
                    }
                }
                
                //AdTemp = AdBuf[2] + AdBuf[3] + AdBuf[4]+ AdBuf[5] + AdBuf[6] + AdBuf[7];
                // AdDp = AdTemp/6;
                AdTemp = AdBuf[1] + AdBuf[2]+ AdBuf[3] + AdBuf[4];
                
                for(i=15;i>0;i--)
                {
                    AdPsBuf[i] = AdPsBuf[i-1]; 
                }
                
                AdPsBuf[0] = AdTemp/4;
                
                if(AdPsCnt<16)
                {
                    AdPsCnt++;
                    AdPs    = AdPsBuf[0];
                    AdTemp  = 0;
                }
                else
                {  
                    for(i=0;i<16;i++)
                    {
                        AdBuf[i] = AdPsBuf[i];
                    }  
                  
                    for(j=0;j<16-1;j++)
                    {
                        for(i=0;i<16-1-j;i++)
                        {
                            if(AdBuf[i]>AdBuf[i+1])    // 数组元素大小按升序排列
                            {
                                temp       = AdBuf[i];
                                AdBuf[i]   = AdBuf[i+1];
                                AdBuf[i+1] = temp;
                            }
                        }
                    }
                   //  AdTemp =  AdBuf[6]+ AdBuf[7] + AdBuf[8]+ AdBuf[9]+ AdBuf[10]+ AdBuf[11] + AdBuf[12]+ AdBuf[13];
                    AdTemp =  AdBuf[5]+ AdBuf[6] + AdBuf[7]+ AdBuf[8]+ AdBuf[9] + AdBuf[10];
                    AdPs   = AdTemp/6;
                    AdTemp = 0;
                }
            }
        }*/
    }
    
    OSIntExit();
}


void TIM2_IRQHandler(void)             //TIM2中断服务函数 
{        
    OS_CPU_SR  cpu_sr;
    
    OS_ENTER_CRITICAL();   // Tell uC/OS-II that we are starting an ISR
    OSIntNesting++;
    OS_EXIT_CRITICAL(); 
    
    if(TIM_GetFlagStatus(TIM2,TIM_FLAG_Update)!=RESET)     
    {                      
        TIM_ClearITPendingBit(TIM2, TIM_IT_Update);  //清除TIMx的中断待处理位  
/*      
        if(AdDpState==1)
        {
            // LedTog(LED2);
            AdDpA     = AdDp;
            // AdDpState = 2;
            AdDpTmr++;
            if(AdDpTmr>=alt_time.test*100)
            {
                AdDpState = 0;
                AdDpTmr   = 0;
                AdDpB     = AdDp;
                // LedTog(LED2);
            }
        }*/
    } 
    
    OSIntExit();
}


void NMI_Handler(void)
{
}

/**
  * @brief  This function handles Hard Fault exception.
  * @param  None
  * @retval None
  */
void HardFault_Handler(void)
{
  /* Go to infinite loop when Hard Fault exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles Memory Manage exception.
  * @param  None
  * @retval None
  */
void MemManage_Handler(void)
{
  /* Go to infinite loop when Memory Manage exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles Bus Fault exception.
  * @param  None
  * @retval None
  */
void BusFault_Handler(void)
{
  /* Go to infinite loop when Bus Fault exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles Usage Fault exception.
  * @param  None
  * @retval None
  */
void UsageFault_Handler(void)
{
  /* Go to infinite loop when Usage Fault exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles SVCall exception.
  * @param  None
  * @retval None
  */
void SVC_Handler(void)
{
}

/**
  * @brief  This function handles Debug Monitor exception.
  * @param  None
  * @retval None
  */
void DebugMon_Handler(void)
{
}

/**
  * @brief  This function handles PendSVC exception.
  * @param  None
  * @retval None
  */
#if 0
void PendSV_Handler(void)
{
}
#endif
/**
  * @brief  This function handles SysTick Handler.
  * @param  None
  * @retval None
  */
#if 0
void SysTick_Handler(void)
{
	OS_CPU_SR cpu_sr;
        
	OS_ENTER_CRITICAL();   // Tell uC/OS-II that we are starting an ISR
	OSIntNesting++;
	OS_EXIT_CRITICAL();
	OSTimeTick();          // Call uC/OS-II's OSTimeTick()
	OSIntExit();
}
#endif
/******************************************************************************/
/*                 STM32F10x Peripherals Interrupt Handlers                   */
/*  Add here the Interrupt Handler for the used peripheral(s) (PPP), for the  */
/*  available peripheral interrupt handler's name please refer to the startup */
/*  file (startup_stm32f10x_xx.s).                                            */
/******************************************************************************/

/**
  * @brief  This function handles PPP interrupt request.
  * @param  None
  * @retval None
  */
/*void PPP_IRQHandler(void)
{
}*/

/**
  * @}
  */ 


/******************* (C) COPYRIGHT 2011 STMicroelectronics *****END OF FILE****/
